/*
 * PrimitiveFeatures.cpp
 *
 *  Created on: Oct 8, 2015
 *      Author: nairb1
 */

#include "CVPrimitiveFeatures.h"

CVPrimitiveFeatures::CVPrimitiveFeatures() {

	// initialization of detectors
	m_detectExtract = cv::KAZE::create();
	m_lineDetect = cv::line_descriptor::LSDDetector::createLSDDetector();

	// initialize the parameters
	m_detectExtract->setThreshold(0.00001);
}

CVPrimitiveFeatures::~CVPrimitiveFeatures() {
	// De-initializing the detectors, keypoints and lines
	clearData();
}

void CVPrimitiveFeatures::clearData(){
	m_kps.clear();
	m_kls.clear();
	m_descriptors.release();

	m_detectExtract->clear();
	m_lineDetect->clear();
}

std::vector<cv::KeyPoint> CVPrimitiveFeatures::getKeyPoints() const{
	return m_kps;
}
std::vector<cv::line_descriptor::KeyLine> CVPrimitiveFeatures::getKeyLines() const{
	return m_kls;
}
cv::Mat CVPrimitiveFeatures::getDescriptors() const{
	return m_descriptors;
}

void CVPrimitiveFeatures::detectFeatures(cv::Mat img){
	// detect all kinds of features and populates the keypoints from the loaded image
	m_detectExtract->detect(img,m_kps);
	m_lineDetect->detect(img,m_kls,1,1);
}
void CVPrimitiveFeatures::computeDescriptors(cv::Mat img){
	// computes all kinds of descriptors from the loaded image
	m_detectExtract->compute(img,m_kps,m_descriptors);
}

