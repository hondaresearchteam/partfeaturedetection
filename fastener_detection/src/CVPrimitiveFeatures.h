/*
 * PrimitiveFeatures.h
 *
 *  Created on: Oct 8, 2015
 *      Author: nairb1
 */

#ifndef CVPRIMITIVEFEATURES_H_
#define CVPRIMITIVEFEATURES_H_

#include <features2d.hpp>
#include <line_descriptor.hpp>
#include <vector>

class CVPrimitiveFeatures {

		// detectors
		cv::Ptr<cv::KAZE> m_detectExtract;
		cv::Ptr<cv::line_descriptor::LSDDetector> m_lineDetect;

		// keypoints
		std::vector<cv::KeyPoint> m_kps;
		std::vector<cv::line_descriptor::KeyLine> m_kls; // key lines

		// storage of descriptors of each type in the image
		cv::Mat m_descriptors;

public:
		CVPrimitiveFeatures();
		virtual ~CVPrimitiveFeatures();

		void detectFeatures(cv::Mat img); // detect all kinds of features and populates the keypoints from the loaded image
		void computeDescriptors(cv::Mat img); // computes all kinds of descriptors from the loaded image

		void clearData(); // clear the image, key points and descriptors

		//get keypoints
		std::vector<cv::KeyPoint> getKeyPoints() const;
		std::vector<cv::line_descriptor::KeyLine> getKeyLines() const;

		cv::Mat getDescriptors() const;

};

#endif /* CVPRIMITIVEFEATURES_H_ */
