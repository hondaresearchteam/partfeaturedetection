/*
 * main.cpp
 *
 *  Created on: Oct 12, 2015
 *      Author: nairb1
 */

#include <iostream>
#include <fstream>
#include <dirent.h>
#include <vector>
#include <string>
#include <vector>
#include <cstring>
#include <fstream>

#include "CVMachinePart.h"

#define TRAINIMGVIEW "Control Image"
#define TESTIMGVIEW "Test Image"
#define RESULTIMGVIEW "Detected Part Feature on test image from left camera"
#define DISPARITY "Rectified Images (left and right) and Disparity map (bottom)"

const float scale_factor = 1; // ideally this shoould be 1 but we are setting it a value to compensate for
// error in the baseline computation

using namespace std;

vector<PartFeatureChar> DetectPartFeatures(string train_img_name,string train_yml_name,string test_img_name,bool displayFlag = true);

int main(int argc,char** argv){

	// Initialize variables
	string train_img_name_abs,train_yml_name_abs, test_img_name_abs, test_img_right_name_abs;
	string test_yml_name_abs;

	// Parse the command line tool : 6 arguments
	if(argc < 4){
		cerr << "Not enough arguments: " << endl;
		exit(1);
	}

	train_img_name_abs = argv[1];
	train_yml_name_abs = argv[2];
	test_img_name_abs = argv[3];

	std::ofstream outputfile;
	outputfile.open(test_yml_name_abs.c_str());

	vector<PartFeatureChar> partfeatures = DetectPartFeatures(train_img_name_abs,train_yml_name_abs,test_img_name_abs);

	for(int k = 0 ; k < partfeatures.size(); k++){

		cout << "(X,Y) = (" <<partfeatures[k].x << " pixels, " << partfeatures[k].y << " pixels, " << endl;
		cout << "Theta : " <<partfeatures[k].theta << " degrees; \t Radius : " << partfeatures[k].radius << " pixels, Area: "<< partfeatures[k].area << " sq.pixels"<< endl;
		//cout << "Height : " << partfeatures[k].height_mm << " mm, Width : " << partfeatures[k].width_mm << " mm" << endl;
	// output these part features to XML
	}

	cv::waitKey(0);
}

vector<PartFeatureChar> DetectPartFeatures(string train_img_name, string roi_ymlfile, string test_img_name, bool displayFlag){

	// Initializing MachinePart
	CVMachinePart* mvp = new CVMachinePart();
	mvp->setDisplayFlag(displayFlag);
	vector<PartFeatureChar> partFeatures;

	// Training the part feature
	cout << "Initialization of structures complete." <<endl;
	cout << "Start training the part feature. " <<endl;
	mvp->trainPartFeatures(train_img_name,roi_ymlfile);
	cout << "Part features trained." <<endl;

	// Test for part feature
	cout << "Testing" << endl;
	partFeatures = mvp->detectPartFeatures(test_img_name);

	cout << "Part features detected" << endl;

	// display results
	if(mvp->isDisplayFlag()){
		mvp->setTrainWindowName(TRAINIMGVIEW);
		mvp->setTestWindowName(TESTIMGVIEW);
		mvp->setResultWindowName(RESULTIMGVIEW);
		mvp->setRegionDetectorColor(cv::Scalar(0,255,0)); // setting green
		mvp->setFeatureMarkerColor(cv::Scalar(0,0,255)); // setting red
		mvp->drawPartFeatures(partFeatures);
		cv::moveWindow(TRAINIMGVIEW,700,0);
		cv::moveWindow(RESULTIMGVIEW,700,500);
		cv::resizeWindow(RESULTIMGVIEW,640,480);
	}

	// TODO: Write up structure to output the detected part feature attributes as an XML file
	cout << "Program completed." <<endl;

	delete mvp;
	return partFeatures;
}



