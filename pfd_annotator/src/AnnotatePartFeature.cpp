/*
 * AnnotatePartFeature.cpp
 *
 *  Created on: Oct 30, 2015
 *      Author: nairb1
 */
#include <iostream>
#include <opencv.hpp>
#include <highgui.hpp>

using namespace std;
using namespace cv;

bool selectObject = false;
Rect selection;
Point origin;
int trackObject = 0;
int finishPartFeature = 0;
Mat image;
Mat annotated_image;
int paintSize = 5;

#define TRAIN_IMAGE_PLATE_1FEET_PATH "/workspace/ATLAS_Project/dataset/train_images_plate_1feet/"
#define TRAIN_IMAGE "/Datasets/KeyenceImageCapture/Analysis1/Train/"
#define TRAIN_XML "/Datasets/KeyenceImageCapture/Analysis1/TrainXML/"

#define WINDOWNAME "Mark Features and press d if satisfied"


// Output YML File
// Number of Features
struct PartFeatureStorage{
	string type;
	cv::Rect roi;
};

string print_msg(){
	return "Press 'q' to exit the program and 'c' to clear the annotations";
}


static void onMouse( int event, int x, int y, int, void* )
{
    if( selectObject )
    {
        selection.x = MIN(x, origin.x);
        selection.y = MIN(y, origin.y);
        selection.width = std::abs(x - origin.x);
        selection.height = std::abs(y - origin.y);

        selection &= Rect(0, 0, image.cols, image.rows);
    }

    switch( event )
    {
    case CV_EVENT_LBUTTONDOWN:
        origin = Point(x,y);
        selection = Rect(x,y,0,0);
        selectObject = true;
        break;
    case CV_EVENT_LBUTTONUP:
        selectObject = false;
        if( selection.width > 0 && selection.height > 0 )
            trackObject = -1;
        break;

//    case CV_EVENT_LBUTTONDBLCLK:
//    	selectObject = false;
//    	if( selection.width > 0 && selection.height > 0 ){
//    		trackObject = -1;
//    		finishPartFeature = 1;
//    	}
//    	break;
    }

}


int main (int argc,char** argv)
{

	string train_img_name, train_img_yml;

	if(argc < 3){
		cerr << "Usage : ./AnnotatePartFeature <train_img> <train_yml_output>" << endl;
		exit(1);
	}

	train_img_name = argv[1];
	train_img_yml = argv[2];

	vector<PartFeatureStorage> partFeatures;

	Mat img = imread(train_img_name);
	namedWindow( WINDOWNAME, WINDOW_NORMAL );
	//resizeWindow(WINDOWNAME,640,480);
	//namedWindow("Selected part features",0);
	setMouseCallback( WINDOWNAME, onMouse, 0 );

	//annotated_image = img.clone();
	img.copyTo(annotated_image);
	cout << print_msg() <<endl;

    for(;;)
    {

    	if( img.empty() )
    		exit(1);
    	//Mat image;
    	annotated_image.copyTo(image);


        //imshow("Selected part features",annotated_image);

        if( selectObject && selection.width > 0 && selection.height > 0)
        {
        	Mat roi(image, selection);
        	bitwise_not(roi, roi);
        	//printf("%d %d %d %d\n", selection.x, selection.y, selection.width, selection.height);
        }

        if(trackObject == -1){
        	if(selection.width > 0 && selection.height > 0)
        		rectangle(image,Point(selection.x,selection.y),Point(selection.x+selection.width,selection.y+selection.height),Scalar(255,0,0),paintSize);
        }

        imshow(WINDOWNAME, image );
//        if(finishPartFeature)
//        	rectangle(image,Point(selection.x,selection.y),Point(selection.x+selection.width,selection.y+selection.height),Scalar(255,0,0));
        char c = waitKey(30);
        if(c == 'd'){

        	rectangle(annotated_image,Point(selection.x,selection.y),Point(selection.x+selection.width,selection.y+selection.height),Scalar(0,255,255),paintSize);
        	annotated_image.copyTo(image);
        	imshow(WINDOWNAME, image );

        	// TODO: populate a vector or a list of part features with name and Rect
        	// For now, just select only circle and xml the 'rect' value
        	cout << "Enter the type of part feature type" << endl;
        	cout << "1) Circle" <<endl;
        	cout << "2) Rectangle" <<endl;
        	cout << "3) Arbitrary" <<endl;
        	cout << "4) Bolt" <<endl;

        	char n = waitKey(0);
        	string type;
        	PartFeatureStorage pf;
        	switch(n){
        	case '1':
        		type = "CIRCLE";
        		cout << "Part feature annotated as " << type <<endl;
        		break;
        	case '2':
        		type = "RECTANGLE";
        		cout << "Part feature annotated as " << type <<endl;
        		break;
        	case '3':
        		type = "ARBITRARY";
        		cout << "Part feature annotated as " << type <<endl;
        		break;
        	case '4':
        		type = "BOLT";
        		cout << "Part feature annotated as " << type <<endl;
        		break;
        	default:
        		cout << "Please make a selection" << endl;
        		continue;
        	}
        	pf.type = type;
        	pf.roi = selection;
        	partFeatures.push_back(pf);

            rectangle(annotated_image,Point(selection.x,selection.y),Point(selection.x+selection.width,selection.y+selection.height),Scalar(0,0,255),paintSize);
        	//fs << name << selection;
        }
        // press this key to clear all annotations
        if(c == 'c'){
        	img.copyTo(annotated_image);
        	partFeatures.clear();
        }

        // press this program to write up all the annotations in an xml file
        if(c == 'q')
        	break;
    }

    // write the set of part features to a YML
    FileStorage fs(train_img_yml,FileStorage::WRITE);
    fs << "numOfPartFeatures" << (int)partFeatures.size();
    fs << "PartFeatures" << "[";
    for(int k =0; k < partFeatures.size(); k++){
    	fs << "{:" << "type" << partFeatures[k].type << "roi" << partFeatures[k].roi << "}";
    }
    fs << "]";

    destroyWindow(WINDOWNAME);
    fs.release();
}


