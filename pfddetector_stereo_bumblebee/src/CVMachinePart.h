/*
 * MachinePart.h
 *
 *  Created on: Oct 8, 2015
 *      Author: nairb1
 */

#ifndef CVMACHINEPART_H_
#define CVMACHINEPART_H_

#include <iostream>
#include <string>
#include <vector>
#include <dirent.h> // for DIR command to read a directory of images in LINUX
#include <iostream>

#include "CVPartFeature.h"
#include "CVPrimitiveFeatures.h"

#define PI 3.1415926535897932384626433832

// enum to set different types of parts
enum PartType{
	PLATE,
	UNKNOWN
};

struct PartFeatureStorage{
	std::string type;
	cv::Rect roi;
};

class CVMachinePart {
private:
	// identification for the part
	std::string m_name;
	PartType m_type;
	int m_numOfPartFeatures;

	// training parameters for each part
	cv::Mat m_trainImg; // to read and analyze a training image
	cv::Mat m_testImg; // to test a sample image

	// for representing the part
	CVPartFeature** m_partFeatures; // part features for the training image
	CVPrimitiveFeatures* m_primitiveFeatures;

	// for GUI/Display
	bool m_displayFlag;
	std::string m_trainWindowName;
	std::string m_testWindowName;
	std::string m_resultWindowName;
	cv::Scalar m_regionDetectorColor; // this is to draw a rectangular region around the part feature with the detected centroid
	cv::Scalar m_featureMarkerColor; // this is draw the outline of the part feature along the boundary

public:
	CVMachinePart();
	virtual ~CVMachinePart();

	// accessor
	std::string getName() const;
	PartType getType() const;
	int getNumOfPartFeatures() const;
	cv::Mat getTrainImg() const;
	cv::Mat getTestImg() const;

	//mutators
	void setName(std::string name);
	void setType(PartType type);
	void setNumOfPartFeatures(int num);
	void setTrainImg(cv::Mat img);
	void setTestImg(cv::Mat img);
	void setDisplayFlag(bool flag);
	void setTrainWindowName(std::string name);
	void setTestWindowName(std::string name);
	void setResultWindowName(std::string name);
	void setRegionDetectorColor(cv::Scalar color);
	void setFeatureMarkerColor(cv::Scalar color);

	// methods
	std::vector<PartFeatureChar> detectPartFeatures(std::string testImgName); // on a single test image
//	std::vector< std::vector<PartFeatureChar> > detectPartFeatures(std::string testImgDir); // on a set of test images

	void initializePartFeatureDescriptors();

	// train Part features
	void trainPartFeatures(std::string trainImgName, std::string trainXMLFile); // using single train image
	//void trainPartFeatures(std::string trainImgDir); // using a set of train images of a particular part to train the part features

	void drawPartFeatures(std::vector<PartFeatureChar> parts);
	void displayMessages(std::vector<PartFeatureChar> parts); // for displaying messages (console or GUI) related to part detection

	bool isDisplayFlag() const {
		return m_displayFlag;
	}
};

#endif /* CVMACHINEPART_H_ */
