/*
 * PartFeature.cpp
 *
 *  Created on: Oct 8, 2015
 *      Author: nairb1
 */

#include "CVPartFeature.h"

CVPartFeature::CVPartFeature() {
	// Initialization
	setType(CIRCLE);
	setNumOfSimilarFeatures(1);
	setUseGPU(false);
	setNumScales(1);
	setNumRotations(1);
	setOffsetRotation(0);

	setNumOfPartFeaturePoints(0);
	setNumOfBackgroundPoints(0);
	setInPaintSize(5);

	//m_primitiveFeatures = new PrimitiveFeatures();
	m_matcher = cv::DescriptorMatcher::create("BruteForce"); // brute force matcher
	setNumNeigbhors(10);

	this->CLAHEEqualize = cv::createCLAHE();
}

CVPartFeature::~CVPartFeature() {
	// deleting and releasing all pointers and structures
	for(int i = 0 ; i < m_numScales; i++)
		m_templateBank[i].clear();
	m_templateBank.clear();

	m_baseTemplate.release();
	m_trainingDataMat.release();
	m_testingDataMat.release();
	m_kpsTrain.clear();
	m_klsTrain.clear();
	m_trainLabels.release();

	m_matcher->clear();

	for(int i = 0; i < m_matchSamplesKNN.size(); i++)
		m_matchSamplesKNN[i].clear();
	m_matchSamplesKNN.clear();
}

// Mutators
FeatureType CVPartFeature::getType() const{
	return m_type;
}
int CVPartFeature::getNumOfSimilarFeatures() const{
	return m_numOfSimilarFeatures;
}

int CVPartFeature::getNumScales() const{
	return m_numScales;
}
int CVPartFeature::getNumRotations() const{
	return m_numRotations;
}
int CVPartFeature::getOffsetRotation() const{
	return m_offsetRotation;
}

cv::Mat CVPartFeature::getTrainingDataMat() const{
	return m_trainingDataMat;
}

std::vector<cv::KeyPoint> CVPartFeature::getKeyPointsTrain() const{
	return m_kpsTrain;
}
std::vector<cv::line_descriptor::KeyLine> CVPartFeature::getKeyLinesTrain() const{
	return m_klsTrain;
}
cv::Rect CVPartFeature::getBoundingBox() const{
	return m_boundingBox;
}
cv::Mat CVPartFeature::getTrainLabels() const{
	return m_trainLabels;
}

cv::Mat CVPartFeature::getTestingDataMat() const{
	return m_testingDataMat;
}

cv::Mat CVPartFeature::getBaseTemplate() const{
	return m_baseTemplate;
}

int CVPartFeature::getInPaintSize() const{
	return m_inPaintSize;
}

// Accessors

void CVPartFeature::setType(FeatureType type){
	m_type = type;
}
void CVPartFeature::setNumOfSimilarFeatures(int num){
	m_numOfSimilarFeatures = num;
}
void CVPartFeature::setUseGPU(bool flag){
	m_useGPU = flag;
}

void CVPartFeature::setNumScales(int n){
	m_numScales = n;
}
void CVPartFeature::setNumRotations(int n){
	m_numRotations = n;
}
void CVPartFeature::setOffsetRotation(float angle){
	m_offsetRotation = angle;
}

void CVPartFeature::setTrainingDataMat(cv::Mat mat){
	m_trainingDataMat = mat.clone();
}

void CVPartFeature::setTestingDataMat(cv::Mat mat){
	m_testingDataMat = mat.clone();
}

void CVPartFeature::setNumNeigbhors(int n){
	m_numNeighbors = n;
}
void CVPartFeature::setKeyPointsTrain(std::vector<cv::KeyPoint> kps){
	m_kpsTrain = kps;
}

void CVPartFeature::setKeyLinesTrain(std::vector<cv::line_descriptor::KeyLine> kls){
	m_klsTrain = kls;
}

void CVPartFeature::setBoundingBox(cv::Rect bbox){
	m_boundingBox = bbox;
}

void CVPartFeature::setNumOfPartFeaturePoints(int n){
	m_numOfPartFeaturePoints =  n;
}
void CVPartFeature::setNumOfBackgroundPoints(int n){
	m_numOfBackgroundPoints = n;
}

void CVPartFeature::setInPaintSize(int n){
	m_inPaintSize = n;
}


void CVPartFeature::createTemplateImgAndTrainLabels(cv::Mat img){
	// routine to generate training labels given the key points and descriptors based on a bounding box
	cv::Mat mask = cv::Mat::zeros(img.rows,img.cols,CV_8UC1);
	cv::Mat detectionImageTr = cv::Mat::zeros(img.rows,img.cols,CV_8UC1);
	cv::rectangle(mask,cv::Point(m_boundingBox.x,m_boundingBox.y),cv::Point(m_boundingBox.x+m_boundingBox.width,m_boundingBox.y+m_boundingBox.height),cv::Scalar(255,255,255),CV_FILLED);

	m_trainLabels = cv::Mat(m_kpsTrain.size(),1,CV_8UC1);
	unsigned char idx;
	for(int k =0; k < m_kpsTrain.size(); k++){
		int x = (int)(m_kpsTrain[k].pt.x);
		int y = (int)(m_kpsTrain[k].pt.y);

		if(mask.at<unsigned char>(y,x) == 255){
			idx = 0;
			m_numOfPartFeaturePoints += 1;
			cv::circle(detectionImageTr,cv::Point(x,y),m_inPaintSize,cv::Scalar(255,255,255),-1);
		}
		else{
			idx = 1;
			m_numOfBackgroundPoints += 1;
		}
		m_trainLabels.at<unsigned char>(k) = idx;
	}

	// Just drawing the lines on the template image
	for(int k = 0 ; k < m_klsTrain.size(); k++){
		cv::line_descriptor::KeyLine line_interest = m_klsTrain[k];

		// Need to check if the end points of the line is inside the mask
		int sx = (int)line_interest.startPointX;
		int sy = (int)line_interest.startPointY;
		int ex = (int)line_interest.endPointX;
		int ey = (int)line_interest.endPointY;
		cv::line(detectionImageTr,cv::Point(sx,sy),cv::Point(ex,ey),cv::Scalar(255,255,255),m_inPaintSize);
	}

	// saving the base template image
	m_baseTemplate = detectionImageTr(m_boundingBox).clone();
}

// generate template bank
void CVPartFeature::generateTemplateBank(cv::Mat templateImg){
	if(templateImg.empty())
		templateImg = m_baseTemplate.clone();
	std::vector<cv::Mat> templateImg_mulRot;
	int degree_change = 360 /m_numRotations;
	//cv::Size rot_size(templateImg.cols + templateImg.cols/4,templateImg.rows + templateImg.rows/4);
	for(unsigned int i = 0 ; i < m_numScales; i++){
		// TODO: downscale the image

		// Store each rotation of template image
		for(unsigned int j = 0 ; j < m_numRotations; j++){
			cv::Mat rotateMatrix = cv::getRotationMatrix2D(cv::Point2f(templateImg.cols/2,templateImg.rows/2),j*degree_change,1);
			//cv::Mat templateImg_rot = cv::Mat::zeros(rot_size.width,rot_size.height,templateImg.type());
			cv::Mat templateImg_rot;
			cv::warpAffine(templateImg,templateImg_rot,rotateMatrix,cv::Size(templateImg.cols,templateImg.rows));
			templateImg_mulRot.push_back(templateImg_rot);
		}

		// store the template rotation bank in a seperate scale
		m_templateBank.push_back(templateImg_mulRot);
	}
}

// primitive feature match
std::vector<cv::KeyPoint> CVPartFeature::primitiveFeatureMatch(std::vector<cv::KeyPoint> kpsTest){
	m_matcher->knnMatch(m_testingDataMat,m_trainingDataMat,m_matchSamplesKNN,m_numNeighbors);
	std::vector<cv::KeyPoint> kpsMatches; // key point matches

	// Test the trained classifier on the same training points
	for(int k = 0; k < m_matchSamplesKNN.size(); k++){
		int query_idx = m_matchSamplesKNN[k][0].queryIdx;
		//end cases
		if(query_idx < 0 || query_idx > m_matchSamplesKNN.size())
			continue;

		int class_idx;
		int count_zeros = 0;
		float avg_dist = 0;
		int count_ones = 0;
		for(int nn = 0 ; nn < m_numNeighbors ; nn++){
			int train_idx = m_matchSamplesKNN[k][nn].trainIdx;
			class_idx = m_trainLabels.at<unsigned char>(train_idx);

			if(class_idx == 0){
				count_zeros += 1;
				avg_dist += m_matchSamplesKNN[k][nn].distance;
			}
			else if(class_idx == 1)
				count_ones += 1;
		}
		avg_dist = avg_dist/m_numNeighbors;
		if(count_zeros > count_ones)
			class_idx = 0;
		else
			class_idx = 1;

		// store the test key points which are classified as "part feature"
		if(class_idx == 0){
			kpsMatches.push_back(kpsTest[query_idx]);
		}
	}

	return kpsMatches;
}

void CVPartFeature::createFeatureMap(cv::Mat img,cv::Mat& featureMapImg,std::vector<cv::KeyPoint> kps, std::vector<cv::line_descriptor::KeyLine> kls){
	// create an empty image
	if(featureMapImg.empty())
		featureMapImg = cv::Mat::zeros(img.rows,img.cols,CV_8UC1);

	// iterating through each keypoint
	for(int i = 0 ; i < kps.size(); i++){
		int x = (int)(kps[i].pt.x);
		int y = (int)(kps[i].pt.y);
		cv::circle(featureMapImg,cv::Point(x,y),m_inPaintSize,cv::Scalar(255,255,255),-1);
	}

	// iterating through each keyline
	for(int i=0; i < kls.size(); i++){

		cv::line_descriptor::KeyLine line_interest = kls[i];
		// Need to check if the end points of the line is inside the mask
		int sx = (int)line_interest.startPointX;
		int sy = (int)line_interest.startPointY;
		int ex = (int)line_interest.endPointX;
		int ey = (int)line_interest.endPointY;
		cv::line(featureMapImg,cv::Point(sx,sy),cv::Point(ex,ey),cv::Scalar(255,255,255),m_inPaintSize);
	}

}

cv::Point3f CVPartFeature::matchTemplateBank(cv::Mat queryImg){

	std::vector<cv::Point3f> optimumPoints; // one optimum point for each scale
	for(int i = 0; i < m_numScales; i++){
		std::vector<cv::Mat> templateImgsRot = m_templateBank[i]; // getting a single scale template bank

		// variables to store the optimum location and direction for a single scale 'i'
		double large_max_value = 0.0;
		cv::Point3d optimum_point;

		// iterating through multiple rotations
		for(int j = 0 ; j < m_numRotations; j++){
			// get the first rotation image
			cv::Mat tempImg = templateImgsRot[j];

			// allocate various matrices and variables to store result
			double minVal, maxVal;
			cv::Point minLoc, maxLoc;
			int result_rows = queryImg.rows - tempImg.rows + 1;
			int result_cols = queryImg.cols - tempImg.cols + 1;
			cv::Mat result_array = cv::Mat::zeros(result_rows,result_cols,CV_32FC1);
			cv::Mat result_array8 = cv::Mat::zeros(result_rows,result_cols,CV_8UC1);

			// match template
			matchTemplate(queryImg,tempImg,result_array,CV_TM_CCOEFF_NORMED);

			// normalize and store in result
			cv::normalize( result_array, result_array8, 0, 1, cv::NORM_MINMAX, -1, cv::Mat() );

			// find the minimum and maximum location and use an offset to get the center
			cv::minMaxLoc(result_array,&minVal,&maxVal,&minLoc,&maxLoc,cv::Mat());
			maxLoc.x = maxLoc.x + (tempImg.cols/2);
			maxLoc.y = maxLoc.y + (tempImg.rows/2);

			if(large_max_value < maxVal){
				large_max_value = maxVal; // setting new largest value
				optimum_point.x = (double)maxLoc.x;
				optimum_point.y = (double)maxLoc.y;
				// the actual angle is measured with respect to the counter-clockwise rotation of the template
				// therefore, the detected region is rotated in the opposite direction
				if(j == 0) //
					optimum_point.z = 0;
				else
					optimum_point.z = 360-(double)(j * 360/m_numRotations);
			}

		}

		optimumPoints.push_back(optimum_point);
	}

	// TODO:selection of the optimum direction across scales
	return optimumPoints[0]; // since only single scale is done
}

cv::Vec3f CVPartFeature::inspectDetectedCircle(cv::Mat detectedFeatureImg){
	cv::Mat img_gray, blurredImg;
	cv::Mat destImg;
	int ksize = 3;
	cv::GaussianBlur(detectedFeatureImg,blurredImg,cv::Size(ksize,ksize),0,0);

	// compute laplacian of the image
	cv::Laplacian(blurredImg,destImg,CV_8UC3);

	std::vector<cv::Vec3f> kCirclesUnprunned;
	int min_dist = 20; // HARD CODING THESE VALUES .TODO: WILL NEED TO INCORPORATE THESE PARAMETERS IN A STRUCTURE
	int high_threshold = 20;
	int false_detection = 20;
	int num_of_circles_to_detect = 1;
	// convert to gray scale
	if(detectedFeatureImg.channels() > 1)
		cv::cvtColor(destImg, img_gray, CV_BGR2GRAY);
	else
		img_gray = destImg.clone();

	// TODO: to better detect the circle in a region of interest,
	cv::HoughCircles(img_gray,kCirclesUnprunned,CV_HOUGH_GRADIENT,1,min_dist,high_threshold,false_detection,0,0);

	// selection of the circle with the largest accumulator values.
	if(num_of_circles_to_detect == -1){
		num_of_circles_to_detect = kCirclesUnprunned.size();
	}

	// if empty return null vector
	if(kCirclesUnprunned.size() == 0)
		return cv::Vec3f();
	else
		return kCirclesUnprunned[0]; // returning the first circle detected

}

// TODO: Inspect detected rectangle, find the corner points
cv::RotatedRect CVPartFeature::inspectDetectedRectangle(cv::Mat detectedFeatureImg) {
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	cv::Mat contourImg = cv::Mat::zeros(detectedFeatureImg.size(),CV_8UC3);
	cv::Mat tempImg = detectedFeatureImg.clone();

	// find the contours
	cv::findContours(tempImg,contours,hierarchy,cv::RETR_TREE,cv::CHAIN_APPROX_SIMPLE,cv::Point(0,0));

	if(contours.size() == 0) // if no contours detected, return empty
		return cv::RotatedRect();

	// return the biggest contour : Assuming that there is only object within it
	cv::RotatedRect mRect = cv::minAreaRect(cv::Mat(contours[0]));

	return mRect;


}

std::vector<float> CVPartFeature::genericShapeFinder(cv::Mat img, cv::Rect roi, FeatureType type){

	// TODO:here, we apply grab cut algorithm if original image
	cv::Rect roiLarge,roiSmall;
	roiLarge.x = roi.x - roi.width/8;
	roiLarge.y = roi.y - roi.height/8;
	roiLarge.width = roi.width + roi.width/4;
	roiLarge.height = roi.height + roi.height/4;

	// ROI CHECK
	if(roiLarge.x < 0)
		roiLarge.x = 0;
	else if(roiLarge.x > img.cols)
		roiLarge.x = img.cols;
	if(roiLarge.y < 0)
		roiLarge.y = 0;
	else if(roiLarge.y > img.rows)
		roiLarge.y = img.rows;

	cv::Mat imgInterest = img(roiLarge);
	roiSmall.x = roi.x - roiLarge.x + roi.width/8;
	roiSmall.y = roi.y - roiLarge.y + roi.height/8;
	roiSmall.width = roi.width - roi.width/4;
	roiSmall.height = roi.height - roi.height/4;

//	cv::Rect roiInterest;
//	roiInterest.x = roiSmall.x + roiSmall.width/2 - 5; //10 pixels
//	roiInterest.y = roiSmall.y + roiSmall.height/2 - 5;
//	roiInterest.width = 10;
//	roiInterest.height = 10;

	//cv::rectangle(imgInterest,cv::Point(roiSmall.x,roiSmall.y),cv::Point(roiSmall.x + roiSmall.width,roiSmall.y+roiSmall.height),cv::Scalar(255,255,255));

	cv::Mat mask = cv::Mat::zeros(imgInterest.rows,imgInterest.cols,CV_8UC1); // mask image which denotes foreground and background pixels
	//cv::Mat mask;
	cv::Mat bgdModel = cv::Mat::zeros(1,65,CV_64FC1);
	cv::Mat fgdModel = cv::Mat::zeros(1,65,CV_64FC1);

//	std::vector<cv::Mat> imgInterestSplit,imgInterestSplitEqualized;
//	cv::split(imgInterest,imgInterestSplit);
//	for(int k = 0 ; k < imgInterestSplit.size(); k++){
//		cv::Mat im1;
//		cv::equalizeHist(imgInterestSplit[k],im1);
//		imgInterestSplitEqualized.push_back(im1);
//	}
//	cv::Mat imgInterestEqualized;
//	cv::merge(imgInterestSplitEqualized,imgInterestEqualized);
	cv::Mat imgInterestGray,imgInterestEqualized;
	cv::cvtColor(imgInterest,imgInterestGray,CV_BGR2GRAY);
	cv::equalizeHist(imgInterestGray,imgInterestGray);
	//this->CLAHEEqualize->apply(imgInterestGray,imgInterestGray);
	//cv::threshold(imgInterestGray,imgInterestGray,10,255,cv::THRESH_OTSU);
	cv::cvtColor(imgInterestGray,imgInterestEqualized,CV_GRAY2BGR);

	//imshow("img",imgInterest);
	//cv::waitKey(0);

	cv::grabCut(imgInterest,mask,roiSmall,bgdModel,fgdModel,1,cv::GC_INIT_WITH_RECT);

	for(int iter = 0; iter < 100; iter ++){
		cv::grabCut(imgInterest,mask,roiSmall,bgdModel,fgdModel,1, cv::GC_EVAL);
	}
//
	cv::Mat maskDisplayFGD = cv::Mat::zeros(imgInterest.rows,imgInterest.cols,CV_8UC1);
	cv::Mat maskDisplayPR_FGD = cv::Mat::zeros(imgInterest.rows,imgInterest.cols,CV_8UC1);
	int num_pixels = 0 ;
	for(int i = 0 ; i < mask.rows ; i++){
		for(int j = 0 ; j < mask.cols;  j++){
			if(mask.at<unsigned char>(i,j) == cv::GC_FGD){
				maskDisplayFGD.at<unsigned char>(i,j) = 255;
				std::cout << "Foreground" << std::endl;
			}
			if(mask.at<unsigned char>(i,j) == cv::GC_PR_FGD){
				maskDisplayPR_FGD.at<unsigned char>(i,j) = 255;
				num_pixels++;
			}
		}
	}

	// TODO: If part feature is a circle, then detect circle
	//namedWindow("Foreground",cv::WINDOW_NORMAL);
	//namedWindow("Possible Foreground",cv::WINDOW_NORMAL);
	//imshow("Foreground",maskDisplayFGD);
	//imshow("Possible Foreground",maskDisplayPR_FGD);
	//cv::waitKey(0);

	//erosion and dilation
	int dilation_type;
	if( type == CIRCLE ){ dilation_type = cv::MORPH_ELLIPSE; }
	  else if( type == RECTANGLE ) { dilation_type = cv::MORPH_RECT; }
	  else { dilation_type = cv::MORPH_CROSS;}

	cv::Mat element = cv::getStructuringElement( dilation_type,cv::Size( 3, 3 ));
	  /// Apply the dilation operation
	cv::erode( maskDisplayPR_FGD, maskDisplayPR_FGD, element );

	std::vector<float> featDims;
	//imshow("maskDisplayPR_FGD",maskDisplayPR_FGD);
	//cv::waitKey(0);
	if(type == CIRCLE && num_pixels!=0){
		cv::Vec3f dim = inspectDetectedCircle(maskDisplayPR_FGD);
		// For circle, its x,y, and radius
		for(int k = 0 ; k < 3; k++){
			featDims.push_back(dim.val[k]);
		}
	}
	else if(type == RECTANGLE && num_pixels !=0){ // check for other feature types
		//TODO: if rectangle, find the area of the rectangle
		cv::RotatedRect dim = inspectDetectedRectangle(maskDisplayPR_FGD);
		//dim.val[0] = num_pixels;
		featDims.push_back(dim.center.x);
		featDims.push_back(dim.center.y);
		featDims.push_back(dim.size.width);
		featDims.push_back(dim.size.height);
		featDims.push_back(dim.angle);
	}
	return featDims;



	// TODO: If its a feature map, then we do not require grab cut

	// TODO: compute the area of the foreground shape

	// TODO: if feature type is a circle then enclose the bounding box region
	// We first find the contours from the binary shape with foreground region detected
	// Then, we apply the fitEllipse method to each contours and min area rectange
	// Then, we obtain the ellipse dimensions. Using analytical formula, compute the area.
	// Output the major and minor axes
	// the rotated rectangle will provide us with the actual orientation
}

std::string featureType2Str(FeatureType type) {
	std::string str1;
	if(type == CIRCLE)
		str1 = "CIRCLE";
	else if(type == RECTANGLE)
		str1 = "RECTANGLE";
	else if(type == ARBITRARY)
		str1 = "ARBITRARY";
	else if(type == BOLT)
		str1 = "BOLT";

	return str1;
}

FeatureType Str2FeatureType(std::string str) {
	FeatureType tp;
	if(str == "CIRCLE")
		tp = CIRCLE;
	else if(str == "RECTANGLE")
		tp = RECTANGLE;
	else if(str == "ARBITRARY")
		tp = ARBITRARY;
	else if(str == "BOLT")
		tp = BOLT;

	return tp;
}
