/*
 * CVStereoVision.h
 *
 *  Created on: Oct 15, 2015
 *      Author: nairb1
 */

#ifndef CVSTEREOVISION_H_
#define CVSTEREOVISION_H

#include <calib3d.hpp>
#include <imgproc.hpp>
#include <highgui.hpp>
#include <iostream>

enum StereoMethod{
	BM, // block matching algorithm
	SGBM // modified H.Hirschmuller algorithm
};

typedef struct{
	double r,g,b;
} COLOR;

class CVStereoVision {

protected:
	StereoMethod type; // method type
	cv::Mat leftImgRectified; // left image
	cv::Mat rightImgRectified; // right image
	cv::Mat leftImgColorRectified; // left image
	cv::Mat rightImgColorRectified; // right image
	cv::Mat leftImgColor;
	cv::Mat rightImgColor;
	cv::Mat disparityMap_BM;
	cv::Mat disparityMap_SGBM;
	cv::Mat zMeasurement;
	cv::Mat image3D;
	cv::Mat canvasImg;
	cv::Mat disparityMapDisplayRGB;

	// parameters for computing depth map (mainly for BM method)
	int numDisparities;
	int blockSize;

	// parameters for computing depth map (for SGBM method)
	int minDisparity;
	int P1;
	int P2;
	int disp12MaxDiff;
	int preFilterCap;
	int uniquenessRatio;
	int speckleWindowSize;
	int speckleRange;
	int mode;
	int windowSize;

	// flags to see if a custom Stereo vision or bumblebee is used
	bool flagBumbleBee;
	bool displayFlag;
	bool flagIntrinsicCalibPresent;
	bool flagExtrinsicCalibPresent;
	bool flagToQueryZ;

	// intrinsic parameters
	cv::Mat cameraMatrixLeft;
	cv::Mat distCoeffLeft;
	cv::Mat cameraMatrixRight;
	cv::Mat distCoeffRight;

	// extrinsic parameters
	cv::Mat rotationMatrix; // rotation matrix between left and right camera coordinate systems
	cv::Mat translationVector; // translation vector between left and right camera coordinate systems
	cv::Mat rotationMatrix0, rotationMatrix1; // rotation matrices for the left and right camera systems
	cv::Mat translationVector0, translationVector1; // translation vectors for the left and right camera systems
	cv::Mat projectionMatrix0, projectionMatrix1; // projection matrices for the left and right cameras
	cv::Mat disparityToDepthMappingMatrix; // Q : Disparity to Depth mapping matrix
	cv::Mat rmap[2][2];

	double baselineDistance; // distance in mm

	std::string m_disparityWindowName;

	// TODO: parameters to store the camera matrix, distortion coefficients for each camera
	// the camera matrix is given as [fx 0 cx ; 0 fy cy; 0 0 1]
	// distortion coefficients :
	// Here, the stereo calibration outputs are loaded from XML file.
	// To obtain the 'z' or disparity in mm, we use the formula
	// Z = baseline * f / (disparityMap + cx(right) - cx (left))
	// baseline : distance between the optical centers of the stereo vision camera
	// cx (right) and cx (left) are obtained after stereo calibration
	// to compute (x,y) in mm, we
	// We should also store the

	// stereo image classs
	cv::Ptr<cv::StereoBM> stereoVisionBM;
	cv::Ptr<cv::StereoSGBM> stereoVisionSGBM;

	cv::Ptr<cv::CLAHE> CLAHEEqualize;

	// functions which needs to be used to obtain 3D coordinates of the point
	// The 3D coordinates must be computed from the disparity map

	// 1) Camera Calibration

	COLOR GetColor(double v, double vmin, double vmax);

public:
	CVStereoVision(); // default constructor
	virtual ~CVStereoVision();
	const cv::Mat& getLeftImgRectified() const;
	void setLeftImgRectified(const cv::Mat& leftImgRectified);
	const cv::Mat& getRightImgRectified() const;
	void setRightImgRectified(const cv::Mat& rightImgRectified);
	StereoMethod getType() const;
	void setType(StereoMethod type);

	// methods
	int getBlockSize() const;
	void setBlockSize(int blockSize);
	int getNumDisparities() const;
	void setNumDisparities(int numDisparities);

	void setCalibrationParameters(float fc, float centerX, float centerY);
	void setStereoImages(cv::Mat leftImg, cv::Mat rightImg);
	const cv::Mat& getDisparityMM();

	void loadCalibrationParameters(std::string intrinsic_filename = "",std::string extrinsic_filename = "");
	void loadStereoImages(std::string leftImgName,std::string rightImgName);
	void initiateLiveCapture(std::string liveCaptureString); // mainly for bumblebee SDK
	void computeDisparity();
	void undistortImages();// uses the camera matrix and stereo rectification parameters to undistort the images
	void displayDisparity();
	void createCanvasImage();

	void convertDisparityToMM();
	cv::Point3d queryXYZMeasurementMM(cv::Point pt);
	void convertDisparityToDistanceUsingBaseline();
	void convertDisparityTo3DImage();
	cv::Point3d queryZMeasurement(cv::Point pt);
	cv::Point3d query3DCoordinates(cv::Point pt);

	const cv::Mat& getDisparityMapBM() const;
	const cv::Mat& getDisparityMapSGBM() const;
	int getDisp12MaxDiff() const;
	void setDisp12MaxDiff(int disp12MaxDiff);
	int getMinDisparity() const;
	void setMinDisparity(int minDisparity);
	int getMode() const;
	void setMode(int mode);
	int getP1() const;
	void setP1(int p1);
	int getP2() const;
	void setP2(int p2);
	int getPreFilterCap() const;
	void setPreFilterCap(int preFilterCap);
	int getSpeckleRange() const;
	void setSpeckleRange(int speckleRange);
	int getSpeckleWindowSize() const;
	void setSpeckleWindowSize(int speckleWindowSize);
	int getUniquenessRatio() const;
	void setUniquenessRatio(int uniquenessRatio);
	int getWindowSize() const;
	void setWindowSize(int windowSize);
	const cv::Mat& getCameraMatrixLeft() const;
	void setCameraMatrixLeft(const cv::Mat& cameraMatrixLeft);
	const cv::Mat& getCameraMatrixRight() const;
	void setCameraMatrixRight(const cv::Mat& cameraMatrixRight);
	const cv::Mat& getDistCoeffLeft() const;
	void setDistCoeffLeft(const cv::Mat& distCoeffLeft);
	const cv::Mat& getDistCoeffRight() const;
	void setDistCoeffRight(const cv::Mat& distCoeffRight);
	bool isFlagBumbleBee() const;
	void setFlagBumbleBee(bool flagBumbleBee);
	const cv::Mat& getLeftImgColor() const;
	void setLeftImgColor(const cv::Mat& leftImgColor);
	const cv::Mat& getRightImgColor() const;
	void setRightImgColor(const cv::Mat& rightImgColor);
	const cv::Mat& getLeftImgColorRectified() const;
	void setLeftImgColorRectified(const cv::Mat& leftImgColorRectified);
	const cv::Mat& getRightImgColorRectified() const;
	void setRightImgColorRectified(const cv::Mat& rightImgColorRectified);
	const std::string& getDisparityWindowName() const;
	void setDisparityWindowName(const std::string& disparityWindowName);
	bool isDisplayFlag() const;
	void setDisplayFlag(bool displayFlag);
	double getBaselineDistance() const;
	void setBaselineDistance(double baselineDistance);
	const cv::Mat& getMeasurement() const;
	void setMeasurement(const cv::Mat& measurement);
	const cv::Mat& getDisparityToDepthMappingMatrix() const;
	void setDisparityToDepthMappingMatrix(
	const cv::Mat& disparityToDepthMappingMatrix);
	const cv::Mat& getProjectionMatrix0() const;
	void setProjectionMatrix0(const cv::Mat& projectionMatrix0);
	const cv::Mat& getProjectionMatrix1() const;
	void setProjectionMatrix1(const cv::Mat& projectionMatrix1);
	const cv::Mat& getRotationMatrix() const;
	void setRotationMatrix(const cv::Mat& rotationMatrix);
	const cv::Mat& getRotationMatrix0() const;
	void setRotationMatrix0(const cv::Mat& rotationMatrix0);
	const cv::Mat& getRotationMatrix1() const;
	void setRotationMatrix1(const cv::Mat& rotationMatrix1);
	const cv::Mat& getTranslationVector() const;
	void setTranslationVector(const cv::Mat& translationVector);
	const cv::Mat& getTranslationVector0() const;
	void setTranslationVector0(const cv::Mat& translationVector0);
	const cv::Mat& getTranslationVector1() const;
	void setTranslationVector1(const cv::Mat& translationVector1);
	const cv::Mat& getDisparityMapDisplayRgb() const;
	void setDisparityMapDisplayRgb(const cv::Mat& disparityMapDisplayRgb);
	bool isFlagIntrinsicCalibPresent() const;
	void setFlagIntrinsicCalibPresent(bool flagIntrinsicCalibPresent);
	bool isFlagExtrinsicCalibPresent() const;
	void setFlagExtrinsicCalibPresent(bool flagExtrinsicCalibPresent);
	bool isFlagToQueryZ() const;
	void setFlagToQueryZ(bool flagToQueryZ);
};

#endif /* CVSTEREOVISION_H_ */
