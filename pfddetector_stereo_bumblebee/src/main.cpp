/*
 * main.cpp
 *
 *  Created on: Oct 12, 2015
 *      Author: nairb1
 */

#include <iostream>
#include <fstream>
#include <dirent.h>
#include <vector>
#include <string>
#include <vector>
#include <cstring>
#include <fstream>

#include "CVMachinePart.h"
#include "CVStereoVision.h"

#define TRAIN_IMAGE_PLATE_1FEET_PATH "/workspace/ATLAS_Project/dataset/train_images_plate_1feet/"
#define TEST_IMAGE_PLATE_1FEET_PATH "/workspace/ATLAS_Project/dataset/test_images_plate_1feet/"
#define TEST_IMAGE_PLATE_1FEET_RESULT_PATH "/workspace/ATLAS_Project/results/test_images_plate_1feet/"

#define TRAIN_IMAGE "/Datasets/KeyenceImageCapture/Analysis1/Train/"
#define TEST_IMAGE "/Datasets/KeyenceImageCapture/Analysis1/Test/"
#define TRAIN_XML "/Datasets/KeyenceImageCapture/Analysis1/TrainXML/"

#define TRAINIMGVIEW "Control Image"
#define TESTIMGVIEW "Test Image"
#define RESULTIMGVIEW "Detected Part Feature on test image from left camera"
#define DISPARITY "Rectified Images (left and right) and Disparity map (bottom)"

const float scale_factor = 1; // ideally this should be 1 but we are setting it a value to compensate for
// error in the baseline computation

using namespace std;

vector<PartFeatureChar> DetectPartFeatures(string train_img_name,string train_yml_name,string test_img_left_name, string test_img_right_name,string intrinsic_filename, string extrinsic_filename, bool displayFlag = true);
cv::Mat CvComputeDisparityFromBumblebee(cv::Mat leftImg, cv::Mat rightImg, vector<double> camParams);

int main(int argc,char** argv){

	// Initialize variables
	string train_img_name_abs,train_yml_name_abs, test_img_left_name_abs, test_img_right_name_abs;
	string test_yml_name_abs;

	// Parse the command line tool : 6 arguments
	if(argc < 6){
		cerr << "Not enough arguments: " << endl;
		exit(1);
	}

	train_img_name_abs = argv[1];
	train_yml_name_abs = argv[2];
	test_img_left_name_abs = argv[3];
	test_img_right_name_abs = argv[4];
	test_yml_name_abs = argv[5];
	string intrinsics_yml_name = "intrinsics.yml";
	string extrinsics_yml_name = "extrinsics.yml";

	std::ofstream outputfile;
	outputfile.open(test_yml_name_abs.c_str());

	vector<PartFeatureChar> partfeatures = DetectPartFeatures(train_img_name_abs,train_yml_name_abs,test_img_left_name_abs, test_img_right_name_abs,intrinsics_yml_name,extrinsics_yml_name);

	for(int k = 0 ; k < partfeatures.size(); k++){

		cout << "World Coordinates : " << featureType2Str(partfeatures[k].type) <<endl;
		cout << "(X,Y,Z) = (" <<partfeatures[k].x_mm << " mm, " << partfeatures[k].y_mm << " mm, " << partfeatures[k].z_mm << " mm)" <<endl;
		cout << "Theta : " <<partfeatures[k].theta << " degrees; \t Radius : " << partfeatures[k].radius_mm << " mm, Area: "<< partfeatures[k].area_mm << " sq.mm"<< endl;
		cout << "Height : " << partfeatures[k].height_mm << " mm, Width : " << partfeatures[k].width_mm << " mm" << endl;
	// output these part features to XML
	}

	// compute the distance between the features
	for(int k = 0 ; k < partfeatures.size(); k++){
		for(int j = k+1; j < partfeatures.size(); j++){
			cv::Point3d pt1(partfeatures[k].x_mm,partfeatures[k].y_mm,partfeatures[k].z_mm);
			cv::Point3d pt2(partfeatures[j].x_mm,partfeatures[j].y_mm,partfeatures[j].z_mm);

			double dist = scale_factor * cv::norm(pt1-pt2);

			char* str;
			std::printf("Distance between feature type %d (%s) and %d (%s) : %f mm\n",k,featureType2Str(partfeatures[k].type).c_str(),j,featureType2Str(partfeatures[j].type).c_str(),dist);

		}
	}

	// output to a file
	for(int k = 0 ; k < partfeatures.size(); k++){

		outputfile << "World Coordinates : " << featureType2Str(partfeatures[k].type) <<endl;
		outputfile << "(X,Y,Z) = (" <<partfeatures[k].x_mm << " mm, " << partfeatures[k].y_mm << " mm, " << partfeatures[k].z_mm << " mm)" <<endl;
		outputfile << "Theta : " <<partfeatures[k].theta << " degrees; \t Radius : " << partfeatures[k].radius_mm << " mm, Area: "<< partfeatures[k].area_mm << " sq.mm"<< endl;
		outputfile << "Height : " << partfeatures[k].height_mm << " mm, Width : " << partfeatures[k].width_mm << " mm" << endl;
	// output these part features to XML
		outputfile << "\n" ;
	}

	// compute the distance between the features
	for(int k = 0 ; k < partfeatures.size(); k++){
		for(int j = k+1; j < partfeatures.size(); j++){
			cv::Point3d pt1(partfeatures[k].x_mm,partfeatures[k].y_mm,partfeatures[k].z_mm);
			cv::Point3d pt2(partfeatures[j].x_mm,partfeatures[j].y_mm,partfeatures[j].z_mm);

			double dist = scale_factor * cv::norm(pt1-pt2);

			char str[200];
			std::sprintf(str,"Distance between feature type %d (%s) and %d (%s) : %f mm\n",k,featureType2Str(partfeatures[k].type).c_str(),j,featureType2Str(partfeatures[j].type).c_str(),dist);

			outputfile << str << endl;
		}
	}

	std::cout << "Press any key to continue" <<std::endl;
	outputfile.close();
	cv::waitKey(0);
}

vector<PartFeatureChar> DetectPartFeatures(string train_img_name, string roi_ymlfile, string test_img_left_name, string test_img_right_name, string intrinsics_filename, string extrinsics_filename, bool displayFlag){

	// TODO: For STEREO VISION
	// Initializing stereo vision
	CVStereoVision* sv = new CVStereoVision();
	sv->setDisplayFlag(displayFlag);
	sv->setFlagBumbleBee(false);
	if(sv->isFlagBumbleBee())
		sv->setBaselineDistance(120.016); // 94 millimetres - this was for Keyence
	else
		sv->setBaselineDistance(78.50); // this is for the point grey
	//120.01600

	cout << "Loading the calibration parameters and stereo set." <<endl;
	// load calibration parameters
	sv->loadCalibrationParameters(intrinsics_filename, extrinsics_filename);

	// load the training and testing image using a method defined in CVStereoVision
	sv->loadStereoImages(test_img_left_name,test_img_right_name);
	cout << "Undistorting the images" << endl;
	sv->undistortImages(); // undistort the images using the calibration parameters

	cout << "Computing the disparity" <<endl;
	// compute the disparity
	sv->computeDisparity();

	cout << "Disparity computed. Converting to MM" <<endl;
	// convert disparity to distance
	sv->convertDisparityToMM();

	// Initializing MachinePart
	CVMachinePart* mvp = new CVMachinePart();
	mvp->setDisplayFlag(displayFlag);
	vector<PartFeatureChar> partFeatures;

	// Training the part feature
	cout << "Initialization of structures complete." <<endl;
	cout << "Start training the part feature. " <<endl;
	mvp->trainPartFeatures(train_img_name,roi_ymlfile);
	cout << "Part features trained." <<endl;

	// Test for part feature
	cout << "Testing" << endl;
	partFeatures = mvp->detectPartFeatures(test_img_left_name);

	cout << "Part features detected" << endl;

	// display
	if(sv->isDisplayFlag()){
		sv->setDisparityWindowName(DISPARITY);
		sv->displayDisparity();
		cv::moveWindow(DISPARITY,0,0);
		cv::resizeWindow(DISPARITY,640,480);
	}

	// Use disparity measurement to get actual x,y,z measurements
	for(int i = 0; i < partFeatures.size(); i++){
		int x = partFeatures[i].x;
		int y = partFeatures[i].y;
		int area = partFeatures[i].area;
		int radius = partFeatures[i].radius;
		int width = partFeatures[i].width;
		int height = partFeatures[i].height;
		float gamma = partFeatures[i].gamma;

		// finding 3D coordinates of the detected part feature
		cv::Point3d pt1 = sv->queryXYZMeasurementMM(cv::Point(x,y));
		partFeatures[i].x_mm = pt1.x;
		partFeatures[i].y_mm = pt1.y;
		partFeatures[i].z_mm = pt1.z;

		// finding the radius in mm

		if(partFeatures[i].type == CIRCLE){
			cv::Point3d pt2 = sv->queryXYZMeasurementMM(cv::Point(x+radius,y));
			double radius = std::sqrt((pt2.x - pt1.x)*(pt2.x - pt1.x) + (pt2.y - pt1.y)*(pt2.y - pt1.y) + (pt2.z - pt1.z)*(pt2.z - pt1.z));
			partFeatures[i].radius_mm = radius*scale_factor;
			partFeatures[i].height_mm = -1;
			partFeatures[i].width_mm = -1;
		}
		else if(partFeatures[i].type == RECTANGLE){
			// TODO:Get the four corners of the rotated rect
			cv::RotatedRect tempRect(cv::Point2f(x,y),cv::Size(11*width/12,11*height/12),gamma);
			cv::Point2f corner_pts[4];
			cv::Point3d corner_pts_3D[4];
			tempRect.points(corner_pts);

			// iterating through each corner points
			for(int k = 0 ; k < 4; k++){
				corner_pts_3D[k] = sv->queryXYZMeasurementMM(cv::Point((int)corner_pts[k].x,(int)corner_pts[k].y));
			}

			// compute the distance between every pair of consecutive points
			int idx0,idx1,idx2;
			idx0 = 0; idx1 = 1; idx2 = 2;
			double dist1 = std::sqrt((corner_pts_3D[idx0].x - corner_pts_3D[idx1].x)*(corner_pts_3D[idx0].x - corner_pts_3D[idx1].x) + (corner_pts_3D[idx0].y - corner_pts_3D[idx1].y)*(corner_pts_3D[idx0].y - corner_pts_3D[idx1].y) + (corner_pts_3D[idx0].z - corner_pts_3D[idx1].z)*(corner_pts_3D[idx0].z - corner_pts_3D[idx1].z));
			double dist2 = std::sqrt((corner_pts_3D[idx1].x - corner_pts_3D[idx2].x)*(corner_pts_3D[idx1].x - corner_pts_3D[idx2].x) + (corner_pts_3D[idx1].y - corner_pts_3D[idx2].y)*(corner_pts_3D[idx1].y - corner_pts_3D[idx2].y) + (corner_pts_3D[idx1].z - corner_pts_3D[idx2].z)*(corner_pts_3D[idx1].z - corner_pts_3D[idx2].z));

			if(dist1 > dist2){ // dist1 is height and dist2 is width
				partFeatures[i].height_mm = 12/11 * dist1 * scale_factor;
				partFeatures[i].width_mm = 12/11 * dist2 * scale_factor;
			}
			else{
				partFeatures[i].height_mm =12/11 * dist2 * scale_factor;
				partFeatures[i].width_mm = 12/11 * dist1 * scale_factor;
			}
//			cv::Point3d pt2 = sv->queryXYZMeasurementMM(cv::Point(x-width/2,y));
//			cv::Point3d pt3 = sv->queryXYZMeasurementMM(cv::Point(x+width/2,y));
//			double width = std::sqrt((pt2.x - pt3.x)*(pt2.x - pt3.x) + (pt2.y - pt3.y)*(pt2.y - pt3.y) + (pt2.z - pt3.z)*(pt2.z - pt3.z));
//			pt2 = sv->queryXYZMeasurementMM(cv::Point(x,y-height/2));
//			pt3 = sv->queryXYZMeasurementMM(cv::Point(x,y+height/2));
//			double height = std::sqrt((pt2.x - pt3.x)*(pt2.x - pt3.x) + (pt2.y - pt3.y)*(pt2.y - pt3.y) + (pt2.z - pt3.z)*(pt2.z - pt3.z));
			partFeatures[i].radius_mm = -1;
		}
		partFeatures[i].area_mm = -1;
	}

	// display results
	if(mvp->isDisplayFlag()){
		mvp->setTrainWindowName(TRAINIMGVIEW);
		mvp->setTestWindowName(TESTIMGVIEW);
		mvp->setResultWindowName(RESULTIMGVIEW);
		mvp->setRegionDetectorColor(cv::Scalar(0,255,0)); // setting green
		mvp->setFeatureMarkerColor(cv::Scalar(0,0,255)); // setting red
		mvp->drawPartFeatures(partFeatures);
		cv::moveWindow(TRAINIMGVIEW,700,0);
		cv::moveWindow(RESULTIMGVIEW,700,500);
		cv::resizeWindow(RESULTIMGVIEW,640,480);
	}

	// TODO: Write up structure to output the detected part feature attributes as an XML file
	cout << "Program completed." <<endl;

	delete mvp;
	delete sv;
	return partFeatures;
}

// function to compute only disparity given the left image, right image, vector of OpenCV-generated intrinsics,
cv::Mat CvComputeDisparityFromBumblebee(cv::Mat leftImg, cv::Mat rightImg, vector<double> camParams){
// camParams[0] - Focal length in pixels
// camParams[1] - Camera Center X
// camParams[2] - Camera Center Y
// camParams[3] - Baseline

	// Base Check
	if(camParams.empty())
	{
		// return NULL
		// No camera parameters
		return cv::Mat();
	}

	// Initializing stereo vision
	float baseLine = camParams[3];
	CVStereoVision* sv = new CVStereoVision();
	sv->setBaselineDistance(baseLine);

	// set the calibration parameters
	sv->setCalibrationParameters(camParams[0],camParams[1],camParams[2]);

	sv->undistortImages(); // undistort the images using the calibration parameters.

	// compute the disparity
	sv->computeDisparity();

	// convert disparity to distance
	sv->convertDisparityToMM();

	return sv->getDisparityMM();

}

