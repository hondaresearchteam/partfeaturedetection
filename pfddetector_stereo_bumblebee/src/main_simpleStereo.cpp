/*
 * main_simpleStereo.cpp
 *
 *  Created on: Feb 2, 2016
 *      Author: nairb1
 */

#include <iostream>
#include <fstream>
#include <dirent.h>
#include <vector>
#include <string>
#include <vector>
#include <cstring>
#include "CVStereoVision.h"

using namespace std;

cv::Mat CvComputeDisparityFromBumblebee(cv::Mat leftImg, cv::Mat rightImg, vector<double> camParams);

int main(int argc, char** argv){

}

// function to compute only disparity given the left image, right image, vector of OpenCV-generated intrinsics,
cv::Mat CvComputeDisparityFromBumblebee(cv::Mat leftImg, cv::Mat rightImg, vector<double> camParams){
// camParams[0] - Focal length in pixels
// camParams[1] - Camera Center X
// camParams[2] - Camera Center Y
// camParams[3] - Baseline

	// Base Check
	if(camParams.empty())
	{
		// return NULL
		// No camera parameters
		return cv::Mat();
	}

	// Initializing stereo vision
	float baseLine = camParams[3];
	CVStereoVision* sv = new CVStereoVision();
	sv->setBaselineDistance(baseLine);

	// set the calibration parameters
	sv->setCalibrationParameters(camParams[0],camParams[1],camParams[2]);

	sv->undistortImages(); // undistort the images using the calibration parameters.

	// compute the disparity
	sv->computeDisparity();

	// convert disparity to distance
	sv->convertDisparityToMM();

	return sv->getDisparityMM();

}



