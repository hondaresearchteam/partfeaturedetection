/*
 * MachinePart.cpp
 *
 *  Created on: Oct 8, 2015
 *      Author: nairb1
 */

#include "CVMachinePart.h"

CVMachinePart::CVMachinePart() {
	// Initialization
	setName("Sample");
	setType(PLATE);
	setNumOfPartFeatures(1); // setting only one part feature
	m_primitiveFeatures = new CVPrimitiveFeatures();
}

CVMachinePart::~CVMachinePart() {
	// release the pointer to part features
	for(int i = 0 ; i < m_numOfPartFeatures; i++){
		delete m_partFeatures[i];
	}
	delete[] m_partFeatures;

	delete m_primitiveFeatures;

	m_trainImg.release();
	m_testImg.release();
}

std::string CVMachinePart::getName() const{
	return m_name;
}
PartType CVMachinePart::getType() const{
	return m_type;
}
int CVMachinePart::getNumOfPartFeatures() const{
	return m_numOfPartFeatures;
}

cv::Mat CVMachinePart::getTrainImg() const{
	return m_trainImg;
}

cv::Mat CVMachinePart::getTestImg() const{
	return m_testImg;
}

//mutators
void CVMachinePart::setName(std::string name){
	m_name = name;
}
void CVMachinePart::setType(PartType type){
	m_type = type;
}
void CVMachinePart::setNumOfPartFeatures(int num){
	m_numOfPartFeatures = num;
}
void CVMachinePart::setTrainImg(cv::Mat img){
	m_trainImg = img.clone();
}
void CVMachinePart::setTestImg(cv::Mat img){
	m_testImg = img.clone();
}

void CVMachinePart::setDisplayFlag(bool flag){
	m_displayFlag = flag;
}
void CVMachinePart::setTrainWindowName(std::string name){
	m_trainWindowName = name;
}
void CVMachinePart::setTestWindowName(std::string name){
	m_testWindowName = name;
}
void CVMachinePart::setResultWindowName(std::string name){
	m_resultWindowName = name;
}

void CVMachinePart::setRegionDetectorColor(cv::Scalar color){
	m_regionDetectorColor = color;
}
void CVMachinePart::setFeatureMarkerColor(cv::Scalar color){
	m_featureMarkerColor = color;
}

// methods
std::vector<PartFeatureChar> CVMachinePart::detectPartFeatures(std::string testImgName){
	std::vector<PartFeatureChar> partFeatureLocations;

	// read the image
	cv::Mat img = cv::imread(testImgName);

	// set the test image
	setTestImg(img);

	// detect the primitive features
	m_primitiveFeatures->detectFeatures(img);

	// compute the descriptors
	m_primitiveFeatures->computeDescriptors(img);

	// get all the keypoints, keylines and descriptors of the train image
	std::vector<cv::KeyPoint> kps = m_primitiveFeatures->getKeyPoints();
	std::vector<cv::line_descriptor::KeyLine> kls = m_primitiveFeatures->getKeyLines();
	cv::Mat descriptors = m_primitiveFeatures->getDescriptors();

	// iterating through each part feature
	// get the keypoints, key lines and descriptors for each type of part feature
		for(int i = 0 ; i < m_numOfPartFeatures; i++){
			// setting the testing data
			m_partFeatures[i]->setTestingDataMat(descriptors);

			// classify the primitive features as a part feature or background
			std::vector<cv::KeyPoint> kps_matches = m_partFeatures[i]->primitiveFeatureMatch(kps);

			// generating feature map
			cv::Mat featureMapImg;
			m_partFeatures[i]->createFeatureMap(img,featureMapImg,kps_matches,kls);

			// multi-template matching
			cv::Point3f detectLoc = m_partFeatures[i]->matchTemplateBank(featureMapImg);
			cv::Rect roi;
			roi.x = detectLoc.x - m_partFeatures[i]->getBaseTemplate().cols/2 ;roi.y = detectLoc.y - m_partFeatures[i]->getBaseTemplate().rows/2;
			roi.height = m_partFeatures[i]->getBaseTemplate().rows; roi.width = m_partFeatures[i]->getBaseTemplate().cols;

			//cv::Mat detectedRegion = m_testImg(roi);

			// TODO: to inspect the circle and measure in pixels
			//cv::Vec3f featDimensions = m_partFeatures[i]->inspectDetectedCircle(detectedRegion);

			std::vector<float> featDimensions = m_partFeatures[i]->genericShapeFinder(img,roi,m_partFeatures[i]->getType());

			PartFeatureChar pf;
			pf.x = int(detectLoc.x);
			pf.y = int(detectLoc.y);
			pf.z = 0;
			pf.theta = int(detectLoc.z);
			pf.type = m_partFeatures[i]->getType();
			pf.width = -1;
			pf.height = -1;
			pf.gamma = -1;
			pf.radius = -1;
			pf.area = -1;

			if(!featDimensions.empty()){ // if featDimensions are empty, then don't get dimensions
				if(pf.type == CIRCLE){
					pf.radius = (int)(featDimensions[2]);
					pf.area = PI * pf.radius * pf.radius;
				}
				else if(pf.type == RECTANGLE){
					pf.width = (int)(featDimensions[2]);
					pf.height = (int)(featDimensions[3]);
					pf.gamma = (int)(featDimensions[4]);
					//pf.area = (int)(featDimensions.val[0]);
					pf.radius = -1;
					pf.area = pf.height * pf.width;
				}
			}
			partFeatureLocations.push_back(pf);
		}

		m_primitiveFeatures->clearData();

		return partFeatureLocations;
}

//std::vector< std::vector<PartFeatureChar> > MachinePart::detectPartFeatures(std::string testImgDir){
//	// a series of test images
//}
// TODO: bounding box must be read from a file. For now, we hard code it in this function
void CVMachinePart::trainPartFeatures(std::string trainImgFile,std::string trainXMLFile){
	// read the image
	cv::Mat img = cv::imread(trainImgFile);

	// read the XML file;
	// open Region of interest XML file for the corresponding train image
	// TODO: This needs to be changed in future to get the region of interest of all part features
	cv::FileStorage fs(trainXMLFile,cv::FileStorage::READ);
	std::vector<PartFeatureStorage> pfs;

	// get the number of part features in the training yml file
	int numOfPartFeatures;
	fs["numOfPartFeatures"] >> numOfPartFeatures;
	this->setNumOfPartFeatures(numOfPartFeatures);
	this->initializePartFeatureDescriptors();// initializing the feature descriptors

	// getting the attribute PartFeatures
	cv::FileNode partFeaturesTrain = fs["PartFeatures"];
	for(cv::FileNodeIterator it = partFeaturesTrain.begin(); it != partFeaturesTrain.end(); it++){
		// get the type of the first feature
		std::string type;
		cv::Rect roi;
		PartFeatureStorage pf;
		(*it)["type"] >> type;
		(*it)["roi"] >> roi;
		pf.type = type;
		pf.roi = roi;
		pfs.push_back(pf);
	}

	fs.release();

	// set the train image
	this->setTrainImg(img);

	// detect the primitive features
	m_primitiveFeatures->detectFeatures(m_trainImg);

	// compute the descriptors
	m_primitiveFeatures->computeDescriptors(m_trainImg);

	// get all the keypoints, keylines and descriptors of the train image
	std::vector<cv::KeyPoint> kps = m_primitiveFeatures->getKeyPoints();
	std::vector<cv::line_descriptor::KeyLine> kls = m_primitiveFeatures->getKeyLines();
	cv::Mat descriptors = m_primitiveFeatures->getDescriptors();

	// get the keypoints, key lines and descriptors for each type of part feature
	for(int i = 0 ; i < m_numOfPartFeatures; i++){

		if(pfs[i].type == "CIRCLE")
			m_partFeatures[i]->setType(CIRCLE); //replacing enum value instead of string
		else if(pfs[i].type == "RECTANGLE")
			m_partFeatures[i]->setType(RECTANGLE);
		else if(pfs[i].type == "ARBITRARY")
			m_partFeatures[i]->setType(ARBITRARY);
		else if(pfs[i].type == "BOLT")
			m_partFeatures[i]->setType(BOLT);

		// setting the keypoints train, keylines and training data
		m_partFeatures[i]->setKeyPointsTrain(kps);
		m_partFeatures[i]->setKeyLinesTrain(kls);
		m_partFeatures[i]->setTrainingDataMat(descriptors);

		// set or manually select the part feature region
		//cv::Rect bbox;
		//bbox.x = 410; bbox.y = 445; bbox.height = 150; bbox.width = 150;
		m_partFeatures[i]->setBoundingBox(pfs[i].roi);

		// create template image and train labels
		m_partFeatures[i]->createTemplateImgAndTrainLabels(m_trainImg);

		// generate the multi-scale multi-rotation template bank
		m_partFeatures[i]->generateTemplateBank();
	}

	// clear the data for the primitive features
	m_primitiveFeatures->clearData();

}

void CVMachinePart::initializePartFeatureDescriptors() {

	// initializing the part feature objects
	m_partFeatures = new CVPartFeature*[m_numOfPartFeatures];
	for(int i = 0 ; i < m_numOfPartFeatures; i++){
		m_partFeatures[i] = new CVPartFeature();
	}
}

//void MachinePart::trainPartFeatures(std::string trainImgDir){
//	// using a set of train images of a particular part to train the part features
//}

void CVMachinePart::drawPartFeatures(std::vector<PartFeatureChar> parts){

	// Show result : Display Images
	cv::Mat resultImg = m_testImg.clone();
	if(m_displayFlag){
		// create windows to see the image
		cv::namedWindow(m_trainWindowName,CV_WINDOW_NORMAL);
		//cv::namedWindow(m_testWindowName,CV_WINDOW_NORMAL);
		cv::namedWindow(m_resultWindowName,CV_WINDOW_NORMAL);

		imshow(m_trainWindowName,m_trainImg);
		cv::waitKey(10);
		//imshow(m_testWindowName,m_testImg);

		//std::cout << "Displaying the control image and test image." << std::endl;
		//std::cout << "Press any key to continue." << std::endl;

		//cv::waitKey(0);

		// display the results on the image
		for(int i=0; i < parts.size(); i++){
			// get the part feature detection statistics ( centroid, radius, other?)
			PartFeatureChar pf = parts[i];

			// get the size of the template image
			cv::Mat tempImg = m_partFeatures[i]->getBaseTemplate();

			// draw a rectangle
			cv::rectangle(resultImg,cv::Point(pf.x-tempImg.cols/2,pf.y-tempImg.rows/2),cv::Point(pf.x+tempImg.cols/2,pf.y+tempImg.rows/2),m_regionDetectorColor,m_partFeatures[i]->getInPaintSize());

			// draw along the feature dimensions
			if(parts[i].type == CIRCLE)
				cv::circle(resultImg,cv::Point(pf.x,pf.y),pf.radius,m_featureMarkerColor,m_partFeatures[i]->getInPaintSize());
			else if(parts[i].type == RECTANGLE){
				cv::Point2f rect_points[4];
				cv::RotatedRect mRect(cv::Point2f(pf.x,pf.y),cv::Size(pf.width,pf.height),pf.gamma);
				mRect.points(rect_points);
				for(int j = 0 ; j < 4; j++){
					cv::line(resultImg,rect_points[j],rect_points[(j+1)%4],m_featureMarkerColor,m_partFeatures[i]->getInPaintSize());
				}
			}

		}

		cv::Point pt1 = cv::Point(resultImg.cols/2 - 5,resultImg.rows/2 - 5);
		cv::Point pt2 = cv::Point(resultImg.cols/2 + 5,resultImg.rows/2 + 5);
		cv::line(resultImg,pt1,pt2,cv::Scalar(255,0,0),3);
		cv::Point pt3 = cv::Point(resultImg.cols/2 + 5,resultImg.rows/2 - 5);
		cv::Point pt4 = cv::Point(resultImg.cols/2 - 5,resultImg.rows/2 + 5);
		cv::line(resultImg,pt3,pt4,cv::Scalar(255,0,0),3);

		imshow(m_resultWindowName,resultImg);
		cv::waitKey(10);
	}

	resultImg.release();

}
