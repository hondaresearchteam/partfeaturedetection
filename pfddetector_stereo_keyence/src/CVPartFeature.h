/*
 * PartFeature.h
 *
 *  Created on: Oct 8, 2015
 *      Author: nairb1
 */

#ifndef CVPARTFEATURE_H_
#define CVPARTFEATURE_H_

#include <core.hpp>
#include <imgproc.hpp>
#include <line_descriptor.hpp>

enum FeatureType{
	CIRCLE,
	RECTANGLE,
	ARBITRARY,
	BOLT
};

// structure which provides the location of the feature, the feature type and the scale factor of the part feature
typedef struct{
	FeatureType type;
	int x; // x-coordinate
	int y; // y-coordinate
	int z; // z-coordinate
	int theta; //in-plane rotation
	int width;
	int height;
	int radius; //pixels
	int area; //pixels
	int gamma; // orientation of the minimum fit rotated rectangle

	double x_mm;
	double y_mm;
	double z_mm;
	double width_mm;
	double height_mm;
	double radius_mm;
	double area_mm;
} PartFeatureChar;

std::string featureType2Str(FeatureType type);
FeatureType Str2FeatureType(std::string str);

class CVPartFeature {

private:
	// identification of feature
	FeatureType m_type;
	bool m_useGPU; // flag to set to use GPU: Currently not implemented
	int m_numOfSimilarFeatures;

	// variables for part feature detection (template matching)
	int m_numScales;
	int m_numRotations;
	float m_offsetRotation; // TODO: parameter which can tell how the object in the template was originally placed
	std::vector< std::vector<cv::Mat> > m_templateBank; //  a template bank which holds different scales and rotations of the plate
	cv::Mat m_baseTemplate;

	// descriptors to store the training/testing data
	cv::Mat m_trainingDataMat, m_testingDataMat;
	std::vector<cv::KeyPoint> m_kpsTrain;
	std::vector<cv::line_descriptor::KeyLine> m_klsTrain;
	cv::Rect m_boundingBox; // bounding box for the image of interest which represents the part feature
	cv::Mat m_trainLabels; // a label of 0 : represents a point belonging to part feature and a label of 1 : represents a point belonging to background
	int m_numOfPartFeaturePoints;
	int m_numOfBackgroundPoints;
	int m_inPaintSize; // size for drawing features

	// matchers
	cv::Ptr<cv::DescriptorMatcher> m_matcher;
	int m_numNeighbors; // for knn match
	std::vector< std::vector<cv::DMatch> > m_matchSamplesKNN;

	cv::Ptr<cv::CLAHE> CLAHEEqualize;

	// TODO: method for circle detection
	// TODO: method for generation of multi-scale multi-rotation template
	// TODO: method for multi-scale multi-rotation template matching

public:
	CVPartFeature();
	virtual ~CVPartFeature();

	// mutators/accessors
	FeatureType getType() const;
	int getNumOfSimilarFeatures() const;
	int getNumScales() const;
	int getNumRotations() const;
	int getOffsetRotation() const;
	cv::Mat getTrainingDataMat() const;
	cv::Mat getTestingDataMat() const;
	cv::Mat getBaseTemplate() const;

	std::vector<cv::KeyPoint> getKeyPointsTrain() const;
	std::vector<cv::line_descriptor::KeyLine> getKeyLinesTrain() const;
	cv::Rect getBoundingBox() const;
	cv::Mat getTrainLabels() const;
	int getInPaintSize() const;

	void setType(FeatureType type);
	void setNumOfSimilarFeatures(int num);
	void setUseGPU(bool flag);
	void setNumScales(int n);
	void setNumRotations(int n);
	void setOffsetRotation(float angle);
	void setTrainingDataMat(cv::Mat mat);
	void setTestingDataMat(cv::Mat mat);
	void setNumOfPartFeaturePoints(int n);
	void setNumOfBackgroundPoints(int n);
	void setInPaintSize(int n);

	void setNumNeigbhors(int n);
	void setKeyPointsTrain(std::vector<cv::KeyPoint> kps);
	void setKeyLinesTrain(std::vector<cv::line_descriptor::KeyLine> kls);
	void setBoundingBox(cv::Rect bbox);

	// methods for template matching
	void createTemplateImgAndTrainLabels(cv::Mat img); // routine to generate training labels given the key points and descriptors based on a bounding box
	void generateTemplateBank(cv::Mat templateImg = cv::Mat());
	std::vector<cv::KeyPoint> primitiveFeatureMatch(std::vector<cv::KeyPoint> kpsTest); // method which creates primitive feature matches
	void createFeatureMap(cv::Mat img,cv::Mat& featureMapImg, std::vector<cv::KeyPoint> kps, std::vector<cv::line_descriptor::KeyLine> kls);
	cv::Point3f matchTemplateBank(cv::Mat queryImg);
	cv::Vec3f inspectDetectedCircle(cv::Mat detectedFeatureImg);
	cv::RotatedRect inspectDetectedRectangle(cv::Mat detectedFeatureImg);
	std::vector<float> genericShapeFinder(cv::Mat detectedFeatureImg, cv::Rect roi,FeatureType type);
};

#endif /* CVPARTFEATURE_H_ */
