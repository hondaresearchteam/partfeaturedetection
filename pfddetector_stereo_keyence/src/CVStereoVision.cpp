/*
 * CVStereoVision.cpp
 *
 *  Created on: Oct 15, 2015
 *      Author: nairb1
 */

#include "CVStereoVision.h"

CVStereoVision::CVStereoVision() {
	// TODO Auto-generated constructor stob
	// set the parameters used in stereo_match.py
	this->setMinDisparity(0);
	this->setWindowSize(5);
	this->setNumDisparities(256-minDisparity); // NOTE: This is the correct disparity setting for this keyence camera setup
	this->setBlockSize(9);
	this->setP1(4*3*blockSize*blockSize);
	this->setP2(16*3*blockSize*blockSize);
	this->setDisp12MaxDiff(0);
	this->setUniquenessRatio(0);
	this->setSpeckleWindowSize(400); //  Can be used to adjust the noisiness in the disparity image
	this->setSpeckleRange(32);
	this->setPreFilterCap(0);
	this->setDisplayFlag(false);

	//stereoVisionBM = cv::StereoBM::create(numDisparities,blockSize);
	this->stereoVisionSGBM = cv::StereoSGBM::create(minDisparity,numDisparities,blockSize,P1,P2,disp12MaxDiff,preFilterCap,uniquenessRatio,speckleWindowSize,speckleRange,cv::StereoSGBM::MODE_SGBM);
	this->CLAHEEqualize = cv::createCLAHE();
}

const cv::Mat& CVStereoVision::getLeftImgRectified() const {
	return leftImgRectified;
}

void CVStereoVision::setLeftImgRectified(const cv::Mat& leftImgRectified) {
	this->leftImgColorRectified = leftImgRectified;
	//cv::pyrDown(leftImgRectified,this->leftImgColorRectified);
	cv::Mat grayImg;
	//if(leftImgRectified.channels() != 1)
		cv::cvtColor(leftImgRectified,grayImg,CV_BGR2GRAY);
	//else
		this->leftImgRectified = grayImg;
}

const cv::Mat& CVStereoVision::getRightImgRectified() const {
	return rightImgRectified;
}

void CVStereoVision::setRightImgRectified(const cv::Mat& rightImgRectified) {
	this->rightImgColorRectified = rightImgRectified;
	//cv::pyrDown(rightImgRectified,this->rightImgColorRectified);
	cv::Mat grayImg;
	//if(leftImgRectified.channels() != 1)
	cv::cvtColor(rightImgRectified,grayImg,CV_BGR2GRAY);
	//else
	this->rightImgRectified = grayImg;

}

StereoMethod CVStereoVision::getType() const {
	return type;
}

int CVStereoVision::getBlockSize() const {
	return blockSize;
}

void CVStereoVision::setBlockSize(int blockSize) {
	this->blockSize = blockSize;
}

int CVStereoVision::getNumDisparities() const {
	return numDisparities;
}

void CVStereoVision::setNumDisparities(int numDisparities) {
	this->numDisparities = numDisparities;
}

void CVStereoVision::setType(StereoMethod type) {
	this->type = type;
}

CVStereoVision::~CVStereoVision() {
	// TODO Auto-generated destructor stub
	this->leftImgRectified.release();
	this->rightImgRectified.release();
	this->disparityMap_BM.release();
	this->disparityMap_SGBM.release();
	this->leftImgColorRectified.release();
	this->rightImgColorRectified.release();
	this->leftImgColor.release();
	this->rightImgColor.release();
}

const cv::Mat& CVStereoVision::getDisparityMapBM() const {
	return disparityMap_BM;
}

int CVStereoVision::getDisp12MaxDiff() const {
	return disp12MaxDiff;
}

void CVStereoVision::setDisp12MaxDiff(int disp12MaxDiff) {
	this->disp12MaxDiff = disp12MaxDiff;
}

int CVStereoVision::getMinDisparity() const {
	return minDisparity;
}

void CVStereoVision::setMinDisparity(int minDisparity) {
	this->minDisparity = minDisparity;
}

int CVStereoVision::getMode() const {
	return mode;
}

void CVStereoVision::setMode(int mode) {
	this->mode = mode;
}

int CVStereoVision::getP1() const {
	return P1;
}

void CVStereoVision::setP1(int p1) {
	P1 = p1;
}

int CVStereoVision::getP2() const {
	return P2;
}

void CVStereoVision::setP2(int p2) {
	P2 = p2;
}

int CVStereoVision::getPreFilterCap() const {
	return preFilterCap;
}

void CVStereoVision::setPreFilterCap(int preFilterCap) {
	this->preFilterCap = preFilterCap;
}

int CVStereoVision::getSpeckleRange() const {
	return speckleRange;
}

void CVStereoVision::setSpeckleRange(int speckleRange) {
	this->speckleRange = speckleRange;
}

int CVStereoVision::getSpeckleWindowSize() const {
	return speckleWindowSize;
}

void CVStereoVision::setSpeckleWindowSize(int speckleWindowSize) {
	this->speckleWindowSize = speckleWindowSize;
}

int CVStereoVision::getUniquenessRatio() const {
	return uniquenessRatio;
}

int CVStereoVision::getWindowSize() const {
	return windowSize;
}

const cv::Mat& CVStereoVision::getCameraMatrixLeft() const {
	return cameraMatrixLeft;
}

void CVStereoVision::setCameraMatrixLeft(const cv::Mat& cameraMatrixLeft) {
	this->cameraMatrixLeft = cameraMatrixLeft;
}

const cv::Mat& CVStereoVision::getCameraMatrixRight() const {
	return cameraMatrixRight;
}

void CVStereoVision::setCameraMatrixRight(const cv::Mat& cameraMatrixRight) {
	this->cameraMatrixRight = cameraMatrixRight;
}

const cv::Mat& CVStereoVision::getDistCoeffLeft() const {
	return distCoeffLeft;
}

void CVStereoVision::setDistCoeffLeft(const cv::Mat& distCoeffLeft) {
	this->distCoeffLeft = distCoeffLeft;
}

const cv::Mat& CVStereoVision::getDistCoeffRight() const {
	return distCoeffRight;
}

bool CVStereoVision::isFlagBumbleBee() const {
	return flagBumbleBee;
}

void CVStereoVision::setFlagBumbleBee(bool flagBumbleBee) {
	this->flagBumbleBee = flagBumbleBee;
}

const cv::Mat& CVStereoVision::getLeftImgColor() const {
	return leftImgColor;
}

void CVStereoVision::setLeftImgColor(const cv::Mat& leftImgColor) {
	this->leftImgColor = leftImgColor;
}

const cv::Mat& CVStereoVision::getRightImgColor() const {
	return rightImgColor;
}

const cv::Mat& CVStereoVision::getLeftImgColorRectified() const {
	return leftImgColorRectified;
}

void CVStereoVision::setLeftImgColorRectified(
		const cv::Mat& leftImgColorRectified) {
	this->leftImgColorRectified = leftImgColorRectified;
}

const cv::Mat& CVStereoVision::getRightImgColorRectified() const {
	return rightImgColorRectified;
}

void CVStereoVision::setRightImgColorRectified(
		const cv::Mat& rightImgColorRectified) {
	this->rightImgColorRectified = rightImgColorRectified;
}

void CVStereoVision::setRightImgColor(const cv::Mat& rightImgColor) {
	this->rightImgColor = rightImgColor;
}

void CVStereoVision::setDistCoeffRight(const cv::Mat& distCoeffRight) {
	this->distCoeffRight = distCoeffRight;
}

void CVStereoVision::setWindowSize(int windowSize) {
	this->windowSize = windowSize;
}

void CVStereoVision::setUniquenessRatio(int uniquenessRatio) {
	this->uniquenessRatio = uniquenessRatio;
}

const cv::Mat& CVStereoVision::getDisparityMapSGBM() const {
	return disparityMap_SGBM;
}

void CVStereoVision::computeDisparity() {

	//apply CLAHE equalization
	cv::Mat lImgGray,rImgGray, lImgGrayEqualized,rImgGrayEqualized, lImgColorEqualized, rImgColorEqualized;
	cv::cvtColor(this->leftImgColorRectified,lImgGray,cv::COLOR_BGR2GRAY);
	cv::cvtColor(this->rightImgColorRectified,rImgGray,cv::COLOR_BGR2GRAY);

	//this->CLAHEEqualize->apply(lImgGray,lImgGrayEqualized);
	//this->CLAHEEqualize->apply(rImgGray,rImgGrayEqualized);

	//cv::cvtColor(lImgGrayEqualized,lImgColorEqualized,cv::COLOR_GRAY2BGR);
	//cv::cvtColor(rImgGrayEqualized,rImgColorEqualized,cv::COLOR_GRAY2BGR);

	stereoVisionSGBM->compute(this->leftImgColorRectified,this->rightImgColorRectified,this->disparityMap_SGBM);
	//stereoVisionSGBM->compute(this->leftImgColor,this->rightImgColor,this->disparityMap_SGBM); // running without calibration
}

void CVStereoVision::undistortImages() {
	cv::Size sizeLeftImg = cv::Size(leftImgColor.cols,leftImgColor.rows);
	cv::Size sizeRightImg = cv::Size(rightImgColor.cols,rightImgColor.rows);
	cv::Size sizeLeftImgNew, sizeRightImgNew;

	if(this->flagIntrinsicCalibPresent & (!this->flagExtrinsicCalibPresent)){ // containing only camera matrices and distortion coefficients
	// get the new camera matrix using the computed distortion coefficients
		cv::Mat cameraMatrixLeftNew = getOptimalNewCameraMatrix(cameraMatrixLeft,distCoeffLeft,sizeLeftImg,0,sizeLeftImgNew);
		cv::Mat cameraMatrixRightNew = getOptimalNewCameraMatrix(cameraMatrixRight,distCoeffRight,sizeLeftImg,0,sizeRightImgNew);

		undistort(leftImgColor,leftImgColorRectified,cameraMatrixLeft,distCoeffLeft,cameraMatrixLeftNew);
		undistort(rightImgColor,rightImgColorRectified,cameraMatrixRight,distCoeffRight,cameraMatrixRightNew);
	}
	else if(this->flagIntrinsicCalibPresent & this->flagExtrinsicCalibPresent){ // custom solution
		// compute the undistort maps
		cv::initUndistortRectifyMap(cameraMatrixLeft,distCoeffLeft,rotationMatrix0,projectionMatrix0,sizeLeftImg,CV_32FC1,rmap[0][0], rmap[0][1]);
		cv::initUndistortRectifyMap(cameraMatrixRight,distCoeffRight,rotationMatrix1,projectionMatrix1,sizeRightImg,CV_32FC1,rmap[1][0], rmap[1][1]);

		remap(leftImgColor,leftImgColorRectified, rmap[0][0], rmap[0][1], cv::INTER_LINEAR,cv::BORDER_TRANSPARENT);
		remap(rightImgColor,rightImgColorRectified, rmap[1][0], rmap[1][1], cv::INTER_LINEAR,cv::BORDER_TRANSPARENT);
	}
	else{
		leftImgColorRectified = leftImgColor.clone();
		rightImgColorRectified = rightImgColor.clone();
	}
}

void CVStereoVision::loadCalibrationParameters(std::string intrinsic_filename,
		std::string extrinsic_filename) {

	cv::FileStorage fs(intrinsic_filename,cv::FileStorage::READ);

	if(fs.isOpened()){
		//
		this->setFlagIntrinsicCalibPresent(true);

		// load the camera matrices and distortion coefficients
		cv::Mat camMat0,camMat1,distCoeffs0,distCoeffs1;
		fs["M1"] >> camMat0;
		fs["M2"] >> camMat1;
		fs["D1"] >> distCoeffs0;
		fs["D2"] >> distCoeffs1;

		// load the camera matrices
		this->setCameraMatrixLeft(camMat0); // load the single camera calibration parameters
		this->setCameraMatrixRight(camMat1);
		this->setDistCoeffLeft(distCoeffs0);
		this->setDistCoeffRight(distCoeffs1);

		fs.release();
	}
	else{
		this->setFlagIntrinsicCalibPresent(false); // no intrinsic flag present
		return ;
	}

	// reading the extrinsic file
	fs.open(extrinsic_filename,cv::FileStorage::READ);
	if(fs.isOpened()){
		this->setFlagExtrinsicCalibPresent(true);

		cv::Mat R,T,R0,T0,P0,R1,T1,P1,Q;
		fs["R"] >> R;
		fs["T"] >> T;
		fs["R1"] >> R0;
		fs["T1"] >> T0;
		fs["P1"] >> P0;
		fs["R2"] >> R1;
		fs["T2"] >> T1;
		fs["P2"] >> P1;
		fs["Q"]>> Q;

		this->setRotationMatrix(R);
		this->setRotationMatrix0(R0);
		this->setRotationMatrix1(R1);
		this->setTranslationVector(T);
		this->setTranslationVector0(T0);
		this->setTranslationVector1(T1);
		this->setProjectionMatrix0(P0);
		this->setProjectionMatrix1(P1);
		this->setDisparityToDepthMappingMatrix(Q);
	}
	else{
		this->setFlagExtrinsicCalibPresent(false);
		return;
	}
}

void CVStereoVision::loadStereoImages(std::string leftImgName,
		std::string rightImgName) {
	cv::Mat leftImg = cv::imread(leftImgName);
	cv::Mat rightImg = cv::imread(rightImgName);

	// load the images
	setLeftImgColor(leftImg);
	setRightImgColor(rightImg);
}

const std::string& CVStereoVision::getDisparityWindowName() const {
	return m_disparityWindowName;
}

bool CVStereoVision::isDisplayFlag() const {
	return displayFlag;
}

void CVStereoVision::displayDisparity() {
	if(this->displayFlag){
		cv::Mat disparityMapDisplayGray;
		// first get the gray scale disparityMap
		// doing this, as I am not sure whats the type in the disparity map generated by opencV
		// so, better to normalize it to unsigned char values (gray scale)
		cv::normalize(disparityMap_SGBM,disparityMapDisplayGray,0,1,CV_MINMAX,CV_64F);

		this->disparityMapDisplayRGB = cv::Mat::zeros(cv::Size(disparityMap_SGBM.cols,disparityMap_SGBM.rows),CV_8UC3);

		int num_of_bands = this->disparityMapDisplayRGB.channels();

		// iterating through each element
		for(int i = 0 ; i < disparityMapDisplayGray.rows; i++){
			unsigned char* rgbRowPtr = this->disparityMapDisplayRGB.ptr<unsigned char>(i);
			double* grayRowPtr = disparityMapDisplayGray.ptr<double>(i);

			for(int j = 0 ; j < disparityMapDisplayGray.cols; j++){
				// get the value
				double val = grayRowPtr[j];

				// get the color coordinates
				COLOR c = GetColor(val,0,1);

				rgbRowPtr[num_of_bands * j + 0] = (unsigned char)(c.b * 255);
				rgbRowPtr[num_of_bands * j + 1] = (unsigned char)(c.g * 255);
				rgbRowPtr[num_of_bands * j + 2] = (unsigned char)(c.r * 255);
			}
		}

		// create the canvas image
		this->createCanvasImage();

		cv::namedWindow(m_disparityWindowName,cv::WINDOW_NORMAL);
		imshow(m_disparityWindowName,this->canvasImg);
		cv::waitKey(10);
	}

}

COLOR CVStereoVision::GetColor(double v, double vmin, double vmax) {

	COLOR c = {0.0, 0.0, 0.0};
	double dv;

	if(v < vmin)
		v = vmin;
	if(v > vmax)
		v = vmax;
	dv = vmax - vmin;

	if( v < (vmin + 0.25 * dv)){
		c.r = 0;
		c.g = 4 * (v - vmin)/dv;
		c.b = 1;
	}
	else if(v < (vmin + 0.5 * dv)){
		c.r = 0;
		c.b = 1 + 4 * (vmin + 0.25 * dv - v)/dv;
		c.g = 1;
	}
	else if(v < (vmin + 0.75 * dv)){
		c.r = 4 * (v - vmin - 0.5 * dv)/dv;
		c.b = 0;
		c.g = 1;
	}
	else{
		c.g = 1 + 4 * (vmin + 0.75 * dv - v)/dv;
		c.b = 0;
		c.r = 1;
	}

	return(c);
}

int CVStereoVision::getBaselineDistance() const {
	return baselineDistance;
}

void CVStereoVision::convertDisparityToDistanceUsingBaseline() {
	zMeasurement = cv::Mat::zeros(disparityMap_SGBM.rows,disparityMap_SGBM.cols,CV_64FC1);
	cv::Mat disparityMapDisplayGray;
	// first get the gray scale disparityMap
	// doing this, as I am not sure whats the type in the disparity map generated by opencV
	// so, better to normalize it to unsigned char values (gray scale)
	normalize(disparityMap_SGBM,disparityMapDisplayGray,0,255,CV_MINMAX,CV_64FC1);

	double fx = cameraMatrixLeft.at<double>(0,0);
	double cx0 = cameraMatrixLeft.at<double>(0,2);
	double cx1 = cameraMatrixRight.at<double>(0,2);

	for(int i = 0 ; i < zMeasurement.rows; i++){
		for(int j = 0 ; j < zMeasurement.cols; j++){
			double dispVal = disparityMapDisplayGray.at<double>(i,j);
			zMeasurement.at<double>(i,j) = fx * baselineDistance / ((double)(dispVal) - (cx1 - cx0));
		}
	}
}

const cv::Mat& CVStereoVision::getMeasurement() const {
	return zMeasurement;
}

const cv::Mat& CVStereoVision::getDisparityToDepthMappingMatrix() const {
	return disparityToDepthMappingMatrix;
}

void CVStereoVision::setDisparityToDepthMappingMatrix(
		const cv::Mat& disparityToDepthMappingMatrix) {
	this->disparityToDepthMappingMatrix = disparityToDepthMappingMatrix;
}

const cv::Mat& CVStereoVision::getProjectionMatrix0() const {
	return projectionMatrix0;
}

void CVStereoVision::setProjectionMatrix0(const cv::Mat& projectionMatrix0) {
	this->projectionMatrix0 = projectionMatrix0;
}

const cv::Mat& CVStereoVision::getProjectionMatrix1() const {
	return projectionMatrix1;
}

void CVStereoVision::setProjectionMatrix1(const cv::Mat& projectionMatrix1) {
	this->projectionMatrix1 = projectionMatrix1;
}

const cv::Mat& CVStereoVision::getRotationMatrix() const {
	return rotationMatrix;
}

void CVStereoVision::setRotationMatrix(const cv::Mat& rotationMatrix) {
	this->rotationMatrix = rotationMatrix;
}

const cv::Mat& CVStereoVision::getRotationMatrix0() const {
	return rotationMatrix0;
}

void CVStereoVision::setRotationMatrix0(const cv::Mat& rotationMatrix0) {
	this->rotationMatrix0 = rotationMatrix0;
}

const cv::Mat& CVStereoVision::getRotationMatrix1() const {
	return rotationMatrix1;
}

void CVStereoVision::setRotationMatrix1(const cv::Mat& rotationMatrix1) {
	this->rotationMatrix1 = rotationMatrix1;
}

const cv::Mat& CVStereoVision::getTranslationVector() const {
	return translationVector;
}

void CVStereoVision::setTranslationVector(const cv::Mat& translationVector) {
	this->translationVector = translationVector;
}

const cv::Mat& CVStereoVision::getTranslationVector0() const {
	return translationVector0;
}

void CVStereoVision::setTranslationVector0(const cv::Mat& translationVector0) {
	this->translationVector0 = translationVector0;
}

const cv::Mat& CVStereoVision::getTranslationVector1() const {
	return translationVector1;
}

cv::Point3d CVStereoVision::query3DCoordinates(cv::Point pt) {
	//float x = pt.x;
	//float y = pt.y;
	//float z = this->image3D.at<float>(pt.y,pt.x);
	cv::Point3d pt3;

	// TODO: convert the point pt to the point after rectification using rmap
	// Need to use only rmap[0][0] rmap[0][1] which corresponds to mapx and mapy of the left image
	cv::Point rectifiedPt;
	if(rmap[0][0].depth() == CV_32FC1){
		rectifiedPt.x = (int)(rmap[0][0].at<float>(pt.y,pt.x));
		rectifiedPt.y = (int)(rmap[0][1].at<float>(pt.y,pt.x));
	}
	else if(rmap[0][0].depth() == CV_16UC1){
		rectifiedPt.x = rmap[0][0].at<unsigned int>(pt.y,pt.x);
	}

	if(this->image3D.depth() == CV_32F){
		cv::Vec3f pVals= this->image3D.at<cv::Vec3f>(rectifiedPt.y,rectifiedPt.x);
		pt3.x = (double)pVals.val[0];
		pt3.y = (double)pVals.val[1];
		pt3.z = (double)pVals.val[2];
	}
	else if(this->image3D.depth() == CV_64F){
		cv::Vec3d pVals= this->image3D.at<cv::Vec3d>(pt.y,pt.x);
		pt3.x = pVals.val[0];
		pt3.y = pVals.val[1];
		pt3.z = pVals.val[2];
	}

	//TODO: CHECK IF THE CORRECT DATA TYPE IS CONSIDERED. ALSO THE DISPARITY MAP FORMAT!!
	return pt3;
}

void CVStereoVision::convertDisparityTo3DImage() {
	cv::Mat disparityMapDisplayGray;
	normalize(disparityMap_SGBM,disparityMapDisplayGray,0,255,CV_MINMAX,CV_8UC1);
	reprojectImageTo3D(disparityMapDisplayGray,this->image3D,this->disparityToDepthMappingMatrix,false);
}

cv::Point3d CVStereoVision::queryZMeasurement(cv::Point pt) {
	double fx = cameraMatrixLeft.at<double>(0,0);
	double z = zMeasurement.at<double>(pt);

	double x = pt.x * z / fx;
	double y = pt.y * z / fx;

	cv::Point3d pt3;
	pt3.x = x;
	pt3.y = y;
	pt3.z = z;

	return pt3;
}

void CVStereoVision::createCanvasImage() {
	this->canvasImg = cv::Mat::zeros(cv::Size(this->leftImgColorRectified.cols+this->rightImgColorRectified.cols,2 * this->disparityMapDisplayRGB.rows),CV_8UC3);

	// set the canvas image
	int block_col = this->leftImgColorRectified.cols;
	int block_row = this->leftImgColorRectified.rows;

	// inputing the left image
	for(int i = 0 ; i < this->leftImgColorRectified.rows; i++){
		unsigned char* rowILeftImg = this->leftImgColorRectified.ptr<unsigned char>(i); // getting the row pointer of the left image
		unsigned char* rowIDestImg = this->canvasImg.ptr<unsigned char>(i);

		for(int j = 0 ; j < this->rightImgColorRectified.cols; j++){
			rowIDestImg[3*j] = rowILeftImg[3*j]; // copying the "blue" color pixel
			rowIDestImg[3*j+1] = rowILeftImg[3*j+1]; // copying the "green" color pixel
			rowIDestImg[3*j+2] = rowILeftImg[3*j+2]; // copying the "yellow" color pixel
		}
	}

	// copying the right image
	for(int i = 0; i < this->rightImgColorRectified.rows; i++){
		unsigned char* rowIRightImg = this->rightImgColorRectified.ptr<unsigned char>(i); // getting the row pointer of the left image
		unsigned char* rowIDestImg = this->canvasImg.ptr<unsigned char>(i);
		int offset = this->leftImgColorRectified.cols * 3;
		for(int j = 0 ; j < this->rightImgColorRectified.cols; j++){
			rowIDestImg[offset + 3*j] = rowIRightImg[3*j]; // copying the "blue" color pixel
			rowIDestImg[offset + 3*j+1] = rowIRightImg[3*j+1]; // copying the "green" color pixel
			rowIDestImg[offset + 3*j+2] = rowIRightImg[3*j+2]; // copying the "yellow" color pixel
		}
	}

	//Mat disparityMap2;
	//resize(disparityMap,disparityMap2,Size(),2,2);
	// copying the disparity at twice the size
	for(int i = 0; i < this->disparityMapDisplayRGB.rows; i++){
		unsigned char* rowIDisparityImg = this->disparityMapDisplayRGB.ptr<unsigned char>(i); // getting the row pointer of the left image
		unsigned char* rowIDestImg = this->canvasImg.ptr<unsigned char>(i + this->leftImgColorRectified.rows);
		int offset = this->leftImgColorRectified.cols * 3/2;
		for(int j = 0 ; j < this->disparityMapDisplayRGB.cols; j++){
			rowIDestImg[offset + 3*j] = rowIDisparityImg[3*j]; // copying the "blue" color pixel
			rowIDestImg[offset + 3*j+1] = rowIDisparityImg[3*j+1]; // copying the "green" color pixel
			rowIDestImg[offset + 3*j+2] = rowIDisparityImg[3*j+2]; // copying the "yellow" color pixel
		}
	}
}

const cv::Mat& CVStereoVision::getDisparityMapDisplayRgb() const {
	return disparityMapDisplayRGB;
}

bool CVStereoVision::isFlagIntrinsicCalibPresent() const {
	return flagIntrinsicCalibPresent;
}

bool CVStereoVision::isFlagExtrinsicCalibPresent() const {
	return flagExtrinsicCalibPresent;
}

void CVStereoVision::convertDisparityToMM() {
	if(this->flagIntrinsicCalibPresent & (!this->flagExtrinsicCalibPresent))
		this->convertDisparityToDistanceUsingBaseline();
	else if(this->flagIntrinsicCalibPresent & (this->flagExtrinsicCalibPresent))
		this->convertDisparityTo3DImage();
}

cv::Point3d CVStereoVision::queryXYZMeasurementMM(cv::Point pt) {
	cv::Point3d pt_mm;
	if(this->flagIntrinsicCalibPresent & (!this->flagExtrinsicCalibPresent))
		pt_mm = this->queryZMeasurement(pt);
	else if(this->flagIntrinsicCalibPresent & this->flagExtrinsicCalibPresent)
		pt_mm = this->query3DCoordinates(pt);
	else
		pt_mm = cv::Point3d(0,0,0);

	return pt_mm;
}

bool CVStereoVision::isFlagToQueryZ() const {
	return flagToQueryZ;
}

void CVStereoVision::initiateLiveCapture(std::string liveCaptureString) {
}

void CVStereoVision::setFlagToQueryZ(bool flagToQueryZ) {
	this->flagToQueryZ = flagToQueryZ;
}

void CVStereoVision::setFlagExtrinsicCalibPresent(
		bool flagExtrinsicCalibPresent) {
	this->flagExtrinsicCalibPresent = flagExtrinsicCalibPresent;
}

void CVStereoVision::setFlagIntrinsicCalibPresent(
		bool flagIntrinsicCalibPresent) {
	this->flagIntrinsicCalibPresent = flagIntrinsicCalibPresent;
}

void CVStereoVision::setDisparityMapDisplayRgb(
		const cv::Mat& disparityMapDisplayRgb) {
	disparityMapDisplayRGB = disparityMapDisplayRgb;
}

void CVStereoVision::setTranslationVector1(const cv::Mat& translationVector1) {
	this->translationVector1 = translationVector1;
}

void CVStereoVision::setMeasurement(const cv::Mat& measurement) {
	zMeasurement = measurement;
}

void CVStereoVision::setBaselineDistance(int baselineDistance) {
	this->baselineDistance = baselineDistance;
}

void CVStereoVision::setDisplayFlag(bool displayFlag) {
	this->displayFlag = displayFlag;
}

void CVStereoVision::setDisparityWindowName(
		const std::string& disparityWindowName) {
	m_disparityWindowName = disparityWindowName;
}
