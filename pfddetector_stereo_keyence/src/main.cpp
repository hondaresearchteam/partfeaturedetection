/*
 * main.cpp
 *
 *  Created on: Oct 12, 2015
 *      Author: nairb1
 */

#include <iostream>
#include <fstream>
#include <dirent.h>
#include <vector>
#include <string>
#include <vector>
#include <cstring>

#include "CVMachinePart.h"
#include "CVStereoVision.h"

#define TRAIN_IMAGE_PLATE_1FEET_PATH "/workspace/ATLAS_Project/dataset/train_images_plate_1feet/"
#define TEST_IMAGE_PLATE_1FEET_PATH "/workspace/ATLAS_Project/dataset/test_images_plate_1feet/"
#define TEST_IMAGE_PLATE_1FEET_RESULT_PATH "/workspace/ATLAS_Project/results/test_images_plate_1feet/"

#define TRAIN_IMAGE "/Datasets/KeyenceImageCapture/Analysis1/Train/"
#define TEST_IMAGE "/Datasets/KeyenceImageCapture/Analysis1/Test/"
#define TRAIN_XML "/Datasets/KeyenceImageCapture/Analysis1/TrainXML/"

#define TRAINIMGVIEW "Control Image"
#define TESTIMGVIEW "Test Image"
#define RESULTIMGVIEW "Detected Part Feature on test image from left camera"
#define DISPARITY "Rectified Images (left and right) and Disparity map (bottom)"

using namespace std;

vector<PartFeatureChar> DetectPartFeatures(string train_img_name,string train_yml_name,string test_img_left_name, string test_img_right_name,string intrinsic_filename, string extrinsic_filename);

int main(int argc,char** argv){

	// create absolute path
	string home_path = getenv("HOME");
	string train_img_path_abs,test_img_path_abs,test_img_result_path_abs,train_xml_path_abs;
	train_img_path_abs = home_path + TRAIN_IMAGE;
	test_img_path_abs = home_path + TEST_IMAGE;
	test_img_result_path_abs = home_path + TEST_IMAGE;
	train_xml_path_abs = home_path + TRAIN_XML;

	cout << "Train Images Path : " << train_img_path_abs.c_str() << endl;
	cout << "Test Images Path : " << test_img_path_abs.c_str() << endl;

	string train_img_name, test_img_left_name, test_img_right_name, train_yml_name, intrinsics_yml_name, extrinsics_yml_name;
	train_img_name = "train_left1.bmp";
	test_img_left_name = "test_left1.bmp";
	test_img_right_name = "test_right1.bmp";
	train_yml_name = "train_left1.yml";
	intrinsics_yml_name = "intrinsics.yml";
	extrinsics_yml_name = "extrinsics.yml";

	string train_img_name_abs = train_img_path_abs + train_img_name;
	string train_yml_name_abs = train_xml_path_abs + train_yml_name;
	string test_img_left_name_abs = test_img_path_abs + test_img_left_name;
	string test_img_right_name_abs = test_img_path_abs + test_img_right_name;

	vector<PartFeatureChar> partfeatures = DetectPartFeatures(train_img_name_abs,train_yml_name_abs,test_img_left_name_abs, test_img_right_name_abs,intrinsics_yml_name,extrinsics_yml_name);

	for(int k = 0 ; k < partfeatures.size(); k++){

		cout << "World Coordinates : " << featureType2Str(partfeatures[k].type) <<endl;
		cout << "(X,Y,Z) = (" <<partfeatures[k].x_mm << " mm, " << partfeatures[k].y_mm << " mm, " << partfeatures[k].z_mm << " mm)" <<endl;
		cout << "Theta : " <<partfeatures[k].theta << " degrees; \t Radius : " << partfeatures[k].radius_mm << " mm, Area: "<< partfeatures[k].area_mm << " sq.mm"<< endl;
		cout << "Height : " << partfeatures[k].height_mm << " mm, Width : " << partfeatures[k].width_mm << " mm" << endl;
	// output these part features to XML
	}

	std::cout << "Press any key to continue" <<std::endl;
	cv::waitKey(0);
}

vector<PartFeatureChar> DetectPartFeatures(string train_img_name, string roi_ymlfile, string test_img_left_name, string test_img_right_name, string intrinsics_filename, string extrinsics_filename){

	// TODO: For STEREO VISION
	// Initializing stereo vision
	CVStereoVision* sv = new CVStereoVision();
	sv->setDisplayFlag(true);
	sv->setBaselineDistance(94); // 94 millimetres
	sv->setFlagBumbleBee(false);

	// load calibration parameters
	sv->loadCalibrationParameters(intrinsics_filename, extrinsics_filename);

	// load the training and testing image using a method defined in CVStereoVision
	sv->loadStereoImages(test_img_left_name,test_img_right_name);
	sv->undistortImages(); // undistort the images using the calibration parameters

	// compute the disparity
	sv->computeDisparity();

	// convert disparity to distance
	//sv->convertDisparityToDistanceUsingBaseline();
	//sv->convertDisparityTo3DImage();
	sv->convertDisparityToMM();

	// display
	if(sv->isDisplayFlag()){
		sv->setDisparityWindowName(DISPARITY);
		sv->displayDisparity();
		cv::moveWindow(DISPARITY,0,0);
		cv::resizeWindow(DISPARITY,640,480);
	}

	// Initializing MachinePart
	CVMachinePart* mvp = new CVMachinePart();
	vector<PartFeatureChar> partFeatures;

	// Training
	mvp->trainPartFeatures(train_img_name,roi_ymlfile);
	mvp->setDisplayFlag(true);

	// Testing
	partFeatures = mvp->detectPartFeatures(test_img_left_name);

	// Use disparity measurement to get actual x,y,z measurements
	for(int i = 0; i < partFeatures.size(); i++){
		int x = partFeatures[i].x;
		int y = partFeatures[i].y;
		int area = partFeatures[i].area;
		int radius = partFeatures[i].radius;
		int width = partFeatures[i].width;
		int height = partFeatures[i].height;

		// finding 3D coordinates of the detected part feature
		cv::Point3d pt1 = sv->queryXYZMeasurementMM(cv::Point(x,y));
		partFeatures[i].x_mm = pt1.x;
		partFeatures[i].y_mm = pt1.y;
		partFeatures[i].z_mm = pt1.z;

		// finding the radius in mm

		if(partFeatures[i].type == CIRCLE){
			cv::Point3d pt2 = sv->queryXYZMeasurementMM(cv::Point(x+radius,y));
			double radius = std::sqrt((pt2.x - pt1.x)*(pt2.x - pt1.x) + (pt2.y - pt1.y)*(pt2.y - pt1.y) + (pt2.z - pt1.z)*(pt2.z - pt1.z));
			partFeatures[i].radius_mm = radius;
			partFeatures[i].height_mm = -1;
			partFeatures[i].width_mm = -1;
		}
		else if(partFeatures[i].type == RECTANGLE){
			cv::Point3d pt2 = sv->queryXYZMeasurementMM(cv::Point(x-1,y));
			double halfWidth = std::sqrt((pt2.x - pt1.x)*(pt2.x - pt1.x) + (pt2.y - pt1.y)*(pt2.y - pt1.y) + (pt2.z - pt1.z)*(pt2.z - pt1.z));
			pt2 = sv->queryXYZMeasurementMM(cv::Point(x,y+1));
			double halfHeight = std::sqrt((pt2.x - pt1.x)*(pt2.x - pt1.x) + (pt2.y - pt1.y)*(pt2.y - pt1.y) + (pt2.z - pt1.z)*(pt2.z - pt1.z));
			partFeatures[i].radius_mm = -1;
			partFeatures[i].height_mm = halfHeight*height;
			partFeatures[i].width_mm = halfWidth*width;
		}
		partFeatures[i].area_mm = -1;
	}

	// display results
	if(mvp->isDisplayFlag()){
		mvp->setTrainWindowName(TRAINIMGVIEW);
		mvp->setTestWindowName(TESTIMGVIEW);
		mvp->setResultWindowName(RESULTIMGVIEW);
		mvp->setRegionDetectorColor(cv::Scalar(0,255,0)); // setting green
		mvp->setFeatureMarkerColor(cv::Scalar(0,0,255)); // setting red
		mvp->drawPartFeatures(partFeatures);
		cv::moveWindow(TRAINIMGVIEW,700,0);
		cv::moveWindow(RESULTIMGVIEW,700,500);
		cv::resizeWindow(RESULTIMGVIEW,640,480);
	}

	// TODO: Write up structure to output the detected part feature attributes as an XML file

	delete mvp;
	delete sv;
	return partFeatures;
}



